
File: crackme.bin contains the original binary

* Creating crackme.ida.asm
  - Load crackme.bin in Ida
  - File -> Produce File -> Create ASM file

     Name: crackme.tmp.asm

     It takes a while (due to large tables)
     Creates a file with 1,671,095 lines (need only 622506 lines)

  - Exit IDA:  "DON"T SAVE DATABASE"

* Truncate crackme.tmp.asm

   head -622506 crackme.tmp.asm > crackme.ida.asm
   rm crackme.tmp.asm

* Creating crackme.od.asm file

     objdump -M intel -d crackme.bin > crackme.od.asm

* Construct initial cfg, partition blocks using liveness analysis

     python ../gencfg/main.py -o cfg -g prolog -i crackme.od.asm

* And memory dump file
     objdump -s crackme.bin > cfg/crackme.memdump.txt
     python ../gencfg/makemem.py cfg/crackme.memdump.txt > cfg/crackme.mem.pl


     
* run swipl

   ?- ['params/load.pl'].
   ?- ['params/program.pl'].


* run program

   ?- run_program.

