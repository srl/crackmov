
enc_password([0xAA, 0xA4, 0x82, 0x4E, 0xA4, 0xFA, 0x1A, 0x82, 0xB6, 0xA4, 0xD8, 0x18, 0xB6, 0x66, 0xAC]).
%enc_password([0xAA, 0x1C, 0xD6, 0x1A, 0x82, 0xA4, 0x5C, 0xA0, 0x7E, 0xD4, 0x66, 0xAC]).
enc_key(0xc0de).

is_crack(Code, Char) :-
    enc_key(Key),
    Prod is Key * Char,
    make_little_endian(Prod,4,[Code|_]),!.

ascii_set(0x20, 0x7e).

code_to_string([], []).


crackit(Crack) :-
    enc_password(Code),
    crack_it(Code, Crack).

crack_it([],[]).
crack_it([Code|Xs], [Crack|Ys]) :-
    ascii_set(Lo, _),
    crack_one(Code, Lo, Crack),
    crack_it(Xs, Ys).

crack_one(Code, Char, 0) :-
    ascii_set(_, Hi),
    Char > Hi, !,
    print([Code, 'not cracker']), nl.

crack_one(Code, Char, Char) :-
    is_crack(Code, Char), !.

crack_one(Code, Char, Result) :-
    Char1 is Char + 1,
    crack_one(Code, Char1, Result).

