
%memtrack(taint, 0x804e220, 13, p).  % enc_password original
memtrack(taint, 0x80f1c2c, 13, c).  % enc_password copy
memtrack(update, 0x80f1c28, 4, bufptr).    % counter/flag 
memtrack(lookup, 0x80f1c40, 13, input).   % buffer for input
memtrack(update, 0x80f1c40, 13, input).   % buffer for input
memtrack(lookup, 0x80f1c2c, 13, encp).  % enc_password copy
memtrack(update, 0x80f1c2c, 13, encp).  % enc_password copy
memtrack(update, 0x80e1cac, 4, switch1). % conditional flag for various selections
%memtrack(update, 0x80d1c90, 4, callptr). % pointer for goto target
memtrack(update, 0x80e1cbc, 4, callselect). 
%% call targets: 

memtrack(instruction, 0x8048f82, to_lower).
memtrack(instruction, 0x8049e06, inc_ptr).
memtrack(instruction, 0x804a1b5, loop1).
memtrack(instruction, 0x804a8e8, loop2).
memtrack(instruction, 0x804b95d, inc_ptr).
memtrack(instruction, 0x804bd0c, setup_loop2).
memtrack(instruction, 0x804c27d, check_pass).
memtrack(instruction, 0x804c99d, print_nope).
memtrack(instruction, 0x804cd43, end_check).
memtrack(instruction, 0x804ceba, exit).
