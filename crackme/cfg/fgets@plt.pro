node('fgets@plt@entry',       block('fgets@plt@entry',[
        i(19, 0x8048210, jmp(dptr(0x804e214+ds)))
      ])).

node('fgets@plt@indirect',       block('fgets@plt@indirect',[

      ])).

node('fgets@plt@000006',       block('fgets@plt@000006',[
        i(20, 0x8048216, push(0x8)),
        i(21, 0x804821b, jmp('printf@plt-0x10@entry'))
      ])).

node('printf@plt-0x10@entry',       block('printf@plt-0x10@entry',[
        i(8, 0x80481f0, push(dptr(0x804e208+ds))),
        i(9, 0x80481f6, jmp(dptr(0x804e20c+ds)))
      ])).

edge('fgets@plt@entry', 'fgets@plt@indirect').

edge('fgets@plt@000006', 'printf@plt-0x10@entry').

