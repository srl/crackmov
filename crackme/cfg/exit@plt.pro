node('exit@plt@entry',       block('exit@plt@entry',[
        i(24, 0x8048220, jmp(dptr(0x804e218+ds)))
      ])).

node('exit@plt@indirect',       block('exit@plt@indirect',[

      ])).

node('exit@plt@000006',       block('exit@plt@000006',[
        i(25, 0x8048226, push(0x10)),
        i(26, 0x804822b, jmp('printf@plt-0x10@entry'))
      ])).

node('printf@plt-0x10@entry',       block('printf@plt-0x10@entry',[
        i(8, 0x80481f0, push(dptr(0x804e208+ds))),
        i(9, 0x80481f6, jmp(dptr(0x804e20c+ds)))
      ])).

edge('exit@plt@entry', 'exit@plt@indirect').

edge('exit@plt@000006', 'printf@plt-0x10@entry').

