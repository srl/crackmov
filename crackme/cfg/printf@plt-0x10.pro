node('printf@plt-0x10@entry',       block('printf@plt-0x10@entry',[
        i(8, 0x80481f0, push(dptr(0x804e208+ds))),
        i(9, 0x80481f6, jmp(dptr(0x804e20c+ds)))
      ])).

node('printf@plt-0x10@indirect',       block('printf@plt-0x10@indirect',[

      ])).

node('printf@plt-0x10@00000c',       block('printf@plt-0x10@00000c',[
        i(10, 0x80481fc, add(bptr(eax), al))
      ])).

edge('printf@plt-0x10@entry', 'printf@plt-0x10@indirect').

