node('printf@plt@entry',       block('printf@plt@entry',[
        i(14, 0x8048200, jmp(dptr(0x804e210+ds)))
      ])).

node('printf@plt@indirect',       block('printf@plt@indirect',[

      ])).

node('printf@plt@000006',       block('printf@plt@000006',[
        i(15, 0x8048206, push(0x0)),
        i(16, 0x804820b, jmp('printf@plt-0x10@entry'))
      ])).

node('printf@plt-0x10@entry',       block('printf@plt-0x10@entry',[
        i(8, 0x80481f0, push(dptr(0x804e208+ds))),
        i(9, 0x80481f6, jmp(dptr(0x804e20c+ds)))
      ])).

edge('printf@plt@000006', 'printf@plt-0x10@entry').

edge('printf@plt@entry', 'printf@plt@indirect').

