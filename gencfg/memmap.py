
# read an objdump file, and generate 'memory' model.
# shape
#     mem(0xAddress, [0xbb, 0xbb, ...]).

import sys
import os
import pickle

class Memory(object):
    def __init__ (self):
        self.memmap = {}

    def update (self, address, value):
        if address == 0: return
        base_address = (address / 16) * 16
        offset = address % 16
        if not base_address in self.memmap.keys ():
            self.memmap[base_address] = [0 for i in range(0,16)]
        self.memmap[base_address][offset] = value

    def lookup (self, address):
        base_address = (address / 16) * 16
        offset = address % 16
        try:
            retval = self.memmap[base_address][offset]
        except:
            print "Error in lookup"
        return retval

    def process_data_line (self, line):
        """
        line is of the form: 
        80482c0 c8fffeff 8910a19c 1c0e088b 15ac1c0e  ................
        """
        words = line.split(" ")
        if not line[0] == " " or len(words) == 1:
            #print line
            return
        print words[1]
        address = int(words[1], 16)
        nibbles = []
        for i in [2,3,4,5]:
            this_word = words[i]
            nibbles += ['0x'+this_word[i:i+2] for i in range(0,len(this_word),2)]

        offset = 0
        for n in nibbles:
            self.update(address+offset, n)
            offset += 1


def process_file (filename):
    memory = Memory()
    fp = open(filename)
    for line in fp:
        memory.process_data_line(line)
    return memory

def pickle_memdump_file (filename, picklefile):
    print "Creating pickle file", picklefile
    memory = process_file (filename)
    ofp = open(picklefile, "wb")
    pickle.dump(memory, ofp)
    ofp.close()
    return memory


def load_pickle_file (picklefile):
    print "reading pickle file", picklefile
    ifp = open(picklefile, "rb")
    memory = pickle.load(ifp)
    ifp.close()
    return memory

data_file = "crackme.memdump.txt"
pickle_file = "crackme.memdump.pickle"

if not os.path.exists(pickle_file):
    memory = pickle_memdump_file(data_file, pickle_file)
else:
    memory = load_pickle_file (pickle_file)

keys = memory.memmap.keys()[0:100]
# print [hex(x) for x in keys]

#for x in keys: print hex(x), memory.memmap[x]
base_address = keys[0]

for i in range(0, 15):
    print hex(base_address+i), memory.lookup(base_address+i)

