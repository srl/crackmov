
# read an objdump file, and generate 'memory' model.
# shape
#     mem(0xAddress, [0xbb, 0xbb, ...]).

import sys
import os

def process_data_line (line):
    """
      line is of the form: 
 80482c0 c8fffeff 8910a19c 1c0e088b 15ac1c0e  ................
    """
    words = line.split(" ")
    if not line[0] == " " or len(words) == 1:
        #print line
        return
    address = "0x"+ words[1]
    nibbles = []
    for i in [2,3,4,5]:
        this_word = words[i]
        nibbles += ['0x'+this_word[i:i+2] for i in range(0,len(this_word),2)]

    print("mem(%s,[%s])." % (address, ",".join(nibbles)))


def process_file (filename):
    fp = open(filename)
    for line in fp:
        process_data_line(line)

import sys
import os
default_filename = "crackme.memdump.txt"
if __name__ == "__main__":
    if len(sys.argv) == 2:
        filename = sys.argv[1]
    elif os.path.exists (default_filename):
        filename = default_filename
    else:
        print "Provide the name of a memdump file, or create ", default_filename
        print "create memdump file using: objdump -s BIN_FILE > MEMDUMP_FILE"
        sys.exit(1)

    process_file(filename)
