#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2470 $
#   $Author: arun $
#   $Date: 2015-06-25 22:03:58 -0500 (Thu, 25 Jun 2015) $


import sys
import os
import asm_ast
import logging
logger = logging.getLogger(__name__)

X86_BASE_REGS = {
 'al': 'eax',
 'ah': 'eax',
 'bl': 'ebx',
 'bh': 'ebx',
 'cl': 'ecx',
 'ch': 'ecx',
 'dl': 'edx',
 'dh': 'edx',
    }

X86_ALIASES = {
  # The order of registers is significant. It's little endian.
  # Low order byte to higher order byte
    'eax': ['al', 'ah','axx'],
    'ebx': ['bl','bh','bxx'],
    'ecx': ['cl','ch','cxx'],
    'edx': ['dl','dh','dxx'],
    'ax': ['al', 'ah'],
    'bx': ['bl', 'bh'],
    'cx': ['cl', 'ch'],
    'dx': ['dl', 'dh']
}

def get_base_reg (reg):
    return X86_BASE_REGS.get(reg, reg)

def get_reg_aliases (reg):
    return set(X86_ALIASES.get(reg, [reg]))

def get_register_operands (op):
    regs = []
    if type(op) == asm_ast.RegisterOperand:
        regs=[op.reg]
    elif type(op) == asm_ast.IndirectOperand:
        if op.reg1 is not None:
            regs.append(op.reg1)
        if op.reg2 is not None:
            regs.append(op.reg2)
        if op.segreg is not None:
            regs.append(op.segreg)
    result = set()
    for reg in regs:
        result |= get_reg_aliases (reg)
    return result

def get_address_operands (op):
    result = []
    if type(op) == asm.ast.ImmediateOperand:
        result = [op.value]
    elif type(op) == asm_ast.ImmediateOffset:
        result = [op.offset]
    elif type(op) == asm_ast.IndirectOperand:
        result = [op.disp]
    return set(result)



REG_MODIFYING_INSTRUCTIONS = ["mov", "add", "sub"]

def get_defuse(instr):
    if not type(instr) == asm_ast.Instruction:
        return {'def':set(), 'use':set()}
    operands = instr.operands
    if instr.mnemonic in REG_MODIFYING_INSTRUCTIONS and type(operands[0]) == asm_ast.RegisterOperand:
        def_regs = get_register_operands(operands[0])
        use_ops = operands[1:]
    else:
        def_regs = set()
        use_ops = operands
    use_regs = set()
    for x in use_ops:
        use_regs |= get_register_operands(x)
    remove_regs = set(["ds", "cs", "ss"])
    return {'def': def_regs-remove_regs, 'use': use_regs-remove_regs}
    

def get_addresses_used (obj):
    result = set()
    if type(obj) == asm_ast.Instruction:
        for op in obj.operands:
            result |= get_address_operands(op)
    elif type(obj) == asm_ast.Block:
        for child in obj.children:
            result |= get_addresses_used (child)
    return result


class Liveness (object):
    def __init__(self, instlist):
        self.instlist = reversed(instlist)
        self.blocklist = []       
        self.new_block = []
        self.published = False
        self.last_live_regs = None
        
    def publish_block (self, block_type, live_regs, msg=""):
        self.blocklist.append((msg+block_type, list(reversed(self.new_block)), live_regs))
        self.new_block = []
        self.published = True
        self.last_live_regs = live_regs
        
    def merge_with_previous (self, live_regs, msg=""):
        prev_message, prev_block, _live_regs  =  self.blocklist[-1]
        merged_block = list(reversed(self.new_block)) + prev_block
        self.blocklist[-1] = (msg+'merged ' + prev_message, merged_block, live_regs)
        self.new_block = []
        self.publish = True
        self.last_live_regs = live_regs
        
    def compute_liveness (self, live_regs):
        # reverse the instruction sequence
        for inst in self.instlist:
            def_use = get_defuse(inst)
            live_regs -= def_use['def']
            live_regs |= def_use['use']
            self.new_block.append(inst)
            param_regs = set(["ebp", "esp", "esi", "edi"])
            message = "--- live %s ----" % list(live_regs)
            if self.published and len(self.new_block) == 1 and live_regs == prev_live_regs:
                self.merge_with_previous (live_regs, msg="1 ")
            elif live_regs == set(["ebp", "esp"]):
                if (self.last_live_regs == set(["ebp"])) or (self.last_live_regs == set(["esp"])):
                    self.merge_with_previous(live_regs, msg="3 ")
                else:
                    self.publish_block(message, live_regs, msg="4 ")
            elif len(live_regs) > 0 and live_regs <= param_regs:
                if len(self.new_block) > 1:
                    self.publish_block(message, live_regs, msg="5 ")
                else:
                    # merge with the previous block that was created
                    self.merge_with_previous(live_regs, msg="6 ")
            elif len(live_regs) == 0:
                self.publish_block("--- nothing live  ---", live_regs, msg="7 ")
            else:
                self.published = False
            prev_live_regs = set(live_regs)
            
    def get_blocks (self):
        return reversed(self.blocklist)
    
