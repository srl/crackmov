#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2486 $
#   $Author: arun $
#   $Date: 2015-07-06 17:55:04 -0500 (Mon, 06 Jul 2015) $

import re
import logging

logger = logging.getLogger('ast')

# Abstract Syntax Tree definition for Intel Assembly.  Though, the
# structure is motivated by objdump parser, it should be useful with
# other disassemblers as well (such as IDA and OllyDbg).

class AST(object):
    """AST super class, just in case there is a need for common
    methods"""
    
    # class properties
    unparser = None
    # support for unparsing
    indent_level = 0

    def __init__(this, name, children):
        if name:
            name = re.sub(r"[\.:\$]", "_", name)
        this.name = name
        this.children = children
        this.parent = None
        this.properties = {}
        this.address = None

        # attach parent pointer back
        for obj in this.children:
            obj.parent = this

    def add_child (this, child):
        this.children.append(child)
        child.parent = this

    def add_child_after (this, new_child, prev_child):
        try:
            pos = this.children.index(prev_child)
            this.children.insert(pos+1, new_child)
            new_child.parent = this
            
        except ValueError:
            logger.error("Badly formed AST - object not in its parents list")

    def next_sibling (this):
        pos = this.parent.children.index(this)
        if pos == len(this.parent.children)-1:
            return None
        return this.parent.children[pos+1]

    def prev_sibling (this):
        pos = this.parent.children.index(this)
        if pos == 0:
            return None
        return this.parent.children[pos-1]

    def rename(this, new_name):
        root = this.root()
        # update the symbol table
        if not root:
            logger.error("object %s is not in AST, cannot update symtab" % this.name)
            return

        symtab = root.symtable
        if not symtab:
            log.error("object %s is in the wrong root. Doesn't have symbol table" % this.name)
            return

        old_name = this.name
        this.name = new_name
        # delete old key
        if old_name in root.symtable: del root.symtable[old_name]
        # create new association
        root.symtable[new_name] = this
        
    def populate_symtab(this, symtab):
        if this.name:
            # only if name is defined
            symtab[this.name] = this
        
        for obj in this.children:
            obj.populate_symtab(symtab)

    def register_special_node (this, name, obj):
        root = this.root()
        if root:
            symtab = root.symtable
            symtab[name] = obj

    def gen_newname (obj):
        """Generate a new name for the procedure/block using distance from the first
        instruction of the unit."""
        proc = obj.parent
        if not proc:
            logger.error("object %s doesn't have a parent" % obj.name)
            return None

        first_inst = proc.first_child_of_type(Instruction)
        if not first_inst:
            logger.error("Parent %s of %s does not start with an Instruction" % (proc.name, obj.name))
            return None

        this_inst = obj.first_child_of_type(Instruction)
        # get the address of the first instruction of the enclosing block
        # and the address of this instruction
        first_address = int(first_inst.address, 16)
        this_address = int(this_inst.address, 16)

        # use the hex value of the difference as a suffix in the name
        distance = this_address - first_address
        suffix = "%0.6x" % distance

        # get the name of the enclosing procedure
        proc_name = proc.name
        # generate name of the block
        new_name = proc_name + "@" +  suffix
        obj.rename(new_name)
        return new_name
    

    def start_address(this):
        """Get the address of the first instruction in this unit"""
        if not this.address is None:
            return this.address
        if len(this.children) == 0:
            return None
        return this.children[0].start_address()
        
    def root(this):
        """To find the root/program object in the AST"""
        return this.parent_of_type(Program)

    def parent_of_type(this, Class):
        """To find the ancestor (really) of given type"""
        parent = this.parent
        if not parent:
            logger.error("No parent of type \"%s\" found for %s" % (Class, this.name))
            return None
        if type(parent) == Class:
            return parent
        return parent.parent_of_type(Class)

    def first_child_of_type(this, Class):
        """To find the leftmost child in the tree, of given type"""
        if len(this.children) == 0:
            return None
        first_child = this.children[0]
        if type(first_child) == Class:
            return first_child
        return first_child.first_child_of_type(Class)

    def last_child_of_type(this, Class):
        """To find the rightmost child in the tree, of given type"""
        if len(this.children) == 0:
            return None
        last_child = this.children[-1]
        if type(last_child) == Class:
            return last_child
        return last_child.last_child_of_type(Class)

    def descendants_of_type(this, Class):
        """To find the leftmost child in the tree, of given type"""
        for child in this.children:
            if type(child) == Class:
                yield child
            for x in iter(child.descendants_of_type(Class)):
                yield x

    def print_outline (this):
        prefix = "".join([" " for x in range(0, this.indent_level)])
        print prefix, type(this), this.name
        for child in this.children:
            if type(child) == Instruction: continue
            child.print_outline()

    def sorted_children (this):
        return sorted(this.children, key=lambda x: x.start_address())
    def __str__(this):
        if this.unparser:
            return this.unparser.unparse(this)
        return None
    
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            

class Program (AST):
    """Root of an ast . Contains a sequence of sections"""
    # class properties
    indent_level = 0
    
    def __init__(this, progname, formatname, sections):
        super(type(this), this).__init__(progname, sections)
        this.formatname = formatname
        this.outside_section = None

        # symbol table: maps names to objects
        this.symtable = {}
        # populate it -- recursively
        this.populate_symtab(this.symtable)

    def get_containing_instruction (this, address):
        """Get instruction that contains this address, if any"""
        symtable = this.symtable
        # convert address to numeric value
        address = int(address, 16)
        for x in range(10):
            # we will check only upto some N addresses
            test_address = hex(address + x)
            if test_address in symtable:
                return symtable[test_address]
        return None

    def create_missing_target (this, target_address):
        """To create program units for representing instructions that are missing.
        This happens when a program has a jump/call to an address, and its not in the
        program space, or is inside another instruction"""
        
        assert not target_address in this.symtable, "create_missing_target called with address of existing instruction"
        
        nearest_instruction = this.get_containing_instruction(target_address)
        if nearest_instruction and (not nearest_instruction.parent.block_type == 'special'):
            # the address is inside another instruction
            # get the procedure containing this address
            containing_procedure = nearest_instruction.parent_of_type(Procedure)
        else:
            # the address is not in the program space
            # Now what... add it to section for outside code
            outside_section = this.outside_section
            if not outside_section:
                outside_section = Section('_outside@sec', [])
                this.children.append(outside_section)
                outside_section.parent = this
                outside_section.populate_symtab(this.symtable)
            
            # create a new procedure to hold the new block
            containing_procedure = Procedure('_outside@proc_'+target_address, [])
            # mark the procedure as a 'dummy_external' procedure
            containing_procedure.dummy_external = True
            
            outside_section.children.append(containing_procedure)
            containing_procedure.parent = outside_section
            containing_procedure.populate_symtab(this.symtable)
            # also force nearest_instruction to null, in case it was in a special block
            nearest_instruction = None
            
        new_block = containing_procedure.add_missing_block(target_address, nearest_instruction)
        # and register it in the symbol table
        new_block.populate_symtab(this.symtable)

class Section (AST):
    """Section - consists of procedures of instruction. Objdump has some logic of breaking
    into procedures. Others may have similar grouping. These groupings are logical, and may
    not play any significant role."""
    # class properties
    indent_level = 1
    def __init__(this, sectionname, procedures):
        # clean up section name. Replace dots with _
        super(type(this), this).__init__(sectionname, procedures)

class Procedure (AST):
    """Procedure is a sequence of instructions, the meat of the program."""
    # class properties
    indent_level = 2

    def __init__(this, procedurename, instructions):
        super(type(this), this).__init__(procedurename, instructions)
        # The children of a procedure may be instructions or blocks
        # each procedure also has special blocks to represent
        # transfer of control from error, indirect control, and return
        # They are created, only if needed, when creating CFG
        # but are not in the list of children
        this._error_block = None
        this._ret_block = None
        this._indirect_target = None
        this._missing_targets = []
        this.dummy_external = False # overridden outside

    def error_block (this):
        if not this._error_block:
            special_name = this.name + "@error"
            this._error_block = Block(special_name, [], 'special')
            this.register_special_node(special_name, this._error_block)
        return this._error_block

    def ret_block (this):
        if not this._ret_block:
            special_name = this.name + "@ret"
            this._ret_block = Block(special_name, [], 'special')
            this.register_special_node(special_name, this._ret_block)
        return this._ret_block

    def indirect_target (this):
        if not this._indirect_target:
            special_name = this.name + "@indirect"
            this._indirect_target = Block(special_name, [], 'special')
            this.register_special_node(special_name, this._indirect_target)
        return this._indirect_target


    def add_missing_block (this, target_address, nearest_instruction):
        # create a dummy jump instruction to the nearest address
        if nearest_instruction:
            # just to model where block where the control would likely go
            nearest_address = nearest_instruction.address
            nearest_line = nearest_instruction.lineno
            lineno = "%s+%d" % (nearest_line, (int(nearest_address, 16) - int(target_address, 16)))
            missing_instruction = Instruction(lineno, target_address, 'FF FF FF', None, 'jmp', [ImmediateOperand(nearest_address)])
        else:
            # or else, just place a NOP instruction there
            missing_instruction = Instruction("xx", target_address, 'FF FF FF', None, 'nop', [])
        special_name = this.name + "@missing" + target_address
        missing_block = Block(special_name, [missing_instruction], 'special')
        this.register_special_node(special_name, missing_block)
        
        # connect it to the tree
        this.children.append(missing_block)
        missing_block.parent = this
        return missing_block


class Block(AST):
    """A block of code"""
    # class properties
    indent_level = 3

    def __init__ (this, blocknum, instructions, block_type=None):
        this.block_type = block_type
        if not blocknum:
            blocknum = 'none'
        super(type(this), this).__init__(str(blocknum), instructions)
        
    def is_special (this):
        if this.block_type and this.block_type == 'special':
            return True
        return False
    def successors (this):
        # get the symbol table to look up labels
        symtab = this.root().symtable
        # get the last instruction in the block
        inst = this.last_child_of_type(Instruction)
        succ_list = []
        # just in case the block is empty
        if not inst: return succ_list
        
        insttype = inst.instruction_type() 
        # get the special cases out of the way
        if insttype == 'ret':
            succ_list.append(this.parent_of_type(Procedure).ret_block())
            return succ_list

        if insttype == 'error':
            succ_list.append(this.parent_of_type(Procedure).error_block())
            return succ_list

        # decide if next block (sibling) is a successor
        sibling = this.next_sibling()
        if sibling:
            # the next block better exist to be a successor
            if insttype == 'call':
                # we'll assume no obfuscation of calls.
                # all calls will return to the caller
                # So need to check if this is one such call
                immoperand = inst.is_immediate_transfer()
                # We can deal with only direct calls
                if immoperand:
                    # get the name of the procedure called (not address)
                    label = immoperand.label
                    # well, I lied. some system calls do not return, such as
                    #   call to exit, or assert_fail.
                    if label and not(label in [ 'exit@plt', '__assert_fail@plt' ]):
                        succ_list.append(sibling)
                    
            elif not insttype == 'jump':
                # otherwise, next block is a successor so long as
                # it is not an unconditional jump.
                succ_list.append(sibling)

        # get successors for jump and jumpifs
        if insttype in ['jump', 'jumpif']:
            immoperand = inst.is_immediate_transfer()
            if immoperand:
                # get the block pointed to by immediate operand
                # label points to block, address/value points to instruction
                label = immoperand.label
                if not label:
                    # no label, for now just do nothing
                    pass
                elif not label in symtab:
                    # the target label is not in the symtab. That's not good news.
                    # it happens when jump/call address is outside the program space
                    # transfers to the middle of an instruction
                    # This should not happen if the AST has been restructured, since
                    # the restructuring process adds dummy nodes... In such case,
                    # the restructuring is buggy. Either new node was not created, or
                    # it was not attached in the symbol table.
                    logger.warning("Block.successors() - Consistency error - no block for label %s" % label)
                else:
                    # get the Block 
                    target = symtab[label]
                    succ_list.append(target)
            else:
                indoperand = inst.is_indirect_transfer()
                if indoperand:
                    # but we do not know the target, so lets get a special target
                    succ_list.append(this.parent_of_type(Procedure).indirect_target())
        # that's it...
        return succ_list

class Instruction (AST):
    # class properties
    indent_level = 4
    
    def __init__(this, lineno, address, code, qual, mnemonic, operands):
        """ lineno -- line number in the disassembled code
        address -- address in the assembly output
        code -- actual sequence of bytes disassembled
        mnemonic -- the operation code
        operands -- list of operands
        qual -- a qualifier -- typically, rep, addr16, etc.
        """
        super(type(this), this).__init__(address, operands)
        this.lineno = lineno
        this.address = address
        this.code = code
        this.qual = qual
        this.mnemonic = mnemonic
        this.operands = operands

    def operand_size (this):
        result = None
        if type(this.operands[0]) == RegisterOperand:
            result =  this.operands[0].register_size()
        if type(this.operands[1]) == RegisterOperand:
            result = this.operands[1].register_size()
        return result
    def instruction_type(this):
        mnemonic = this.mnemonic
        if mnemonic in ['call', 'ret', 'error']:
            return mnemonic
        if mnemonic == "jmp":
            return 'jump'
        if re.match(r"j.*", mnemonic):
            return 'jumpif'
        return 'other'

    def is_immediate_transfer(this):
       if not this.instruction_type() in ['call', 'jump', 'jumpif']:
           return None
       # so instruction is either call, jump, or jump conditional
       if not len(this.operands) == 1:
           # it should have one and only one argument
           # NOTE: objdump disassembly error (or bad code) may result into
           #    no argument. So lets just keep quiet about it
           logger.debug("Instruction must have only one argument %s" % this)
           return None
       operand = this.operands[0]
       if not type(operand) == ImmediateOperand:
           return None
       return operand
       
    def is_indirect_transfer(this):
       instype = this.instruction_type()
       if instype == 'ret':
           return True
       if not this.instruction_type() in ['call', 'jump', 'jumpif' ]:
           return None
       # Must be a call, jump, or jumpif 
       if not len(this.operands) == 1:
           # it should have one and only one argument
           # This could happen if the disasm parser couldn't parse the argument.
           # It could also happen if the disassembly is buggy.
           logger.debug("Instruction must have only one argument %s" % this)
           return None
       operand = this.operands[0]
       if type(operand) == ImmediateOperand:
           # and it should not be an Immediate Operand
           return None
       return operand
       
class DirectOperand(AST):
    """Base class for all direct operands."""
    def __init__(this):
        super(DirectOperand, this).__init__(None, [])

REGSIZE={
  'ah':'byte', 'al':'byte', 'bl':'byte', 'bh':'byte', 'cl':'byte', 'ch':'byte', 'dl':'byte', 'dh':'byte',
  'ax':'word', 'bx':'word', 'cx':'word', 'dx':'word'
}
class RegisterOperand (DirectOperand):
    """Register operands. Just the name of a register"""
    def __init__ (this, reg):
        super(type(this), this).__init__()
        this.reg = reg
    def register_size(this):
        return REGSIZE.get(this.reg, 'dword')
        

class ImmediateOperand (DirectOperand):
    """Immediate numeric value. Size of value may depend on operations.
    In objdump, value may be of the form 0xnn or nn. The nn in both cases
    appear to represent hex number. This needs to be verified, though."""
    
    def __init__ (this, value, label=None):
        super(type(this), this).__init__()
        this.value = value
        this.label = label

class ImmediateOffset (DirectOperand):
    """This is an index+offset type of direct operand. Its appears to be used in
    call and jump like instructions."""
    
    def __init__ (this, offset, disp):
        super(type(this), this).__init__()
        this.offset = offset
        this.disp = disp

class IndirectOperand (AST):
    """A fully qualified indirect operand. Not all of the components are always present.
    A missing component has either a default value, or no value. The default value is
    determined by the instruction used, and is left for the interpreter to determine."""
    def __init__ (this, ptr, segreg, reg1, reg2, xplier, disp):
        super(type(this), this).__init__(None, [])
        this.ptr = ptr
        this.segreg = segreg
        this.reg1 = reg1
        this.reg2 = reg2
        this.xplier = xplier
        this.disp = disp

