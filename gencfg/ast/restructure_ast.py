#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2470 $
#   $Author: arun $
#   $Date: 2015-06-25 22:03:58 -0500 (Thu, 25 Jun 2015) $


import sys
import os
import asm_ast
import logging
logger = logging.getLogger(__name__)
import liveness

def restructure(ast):
    logger.debug("-------- RESTRUCTURING AST ------------")
    # mark the targets of CALL/JUMP instructions
    mark_targets(ast)
    # create procedures
    split_code_in_procedures(ast)
    # wrap each procedures code in block
    wrap_code_in_blocks(ast)
    # deal with missing targets of CALL/JUMP
    add_blocks_for_missing_targets(ast)
    mark_targets(ast)
    # create blocks for jump targets
    split_code_in_blocks(ast)
    # create blocks by splitting at jump instructions
    split_x(ast)
    # split_special_move(ast)
    split_liveness(ast)
    # give new labels to procedures and blocks
    relabel_transfers(ast)


def get_direct_transfers(ast):
    symtable = ast.symtable
    result = []
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Instruction:
            continue
        if not obj.is_immediate_transfer():
            continue
        result.append(obj)
    return result


def add_blocks_for_missing_targets(ast):
    """Add special blocks to represent transfer of control for addresses
    at which there is no instruction, either because the address is outside
    program space or the address is inside another instruction"""
    
    transfers = get_direct_transfers(ast)
    # will update the symbol table too this time

    for obj in transfers:
        operand = obj.operands[0]
        if not type(operand) == asm_ast.ImmediateOperand:
            continue
        # so this is a call or jump instruction with direct address
        target_address = operand.value
        if not target_address in ast.symtable:
            ast.create_missing_target (target_address)

def mark_targets (ast):
    """Mark targets of call and jump instructions"""
    transfers = get_direct_transfers(ast)
    symtable = ast.symtable
    for obj in transfers:
        operand = obj.operands[0]
        if not type(operand) == asm_ast.ImmediateOperand:
            continue
        # so this is a call or jump instruction with direct address
        target_address = operand.value
        if not target_address in symtable:
            # there is no statement at that address
            # either because the address is outside the program
            # space, or the address is inside an instruction
            # Ignore, we will be adding new blocks later.
            # print "Error - Target address %s missing; shouldn't happen since we added blocks" % target_address
            pass
            
        else:
            target = symtable[target_address]
            instype = obj.instruction_type()
            target.properties['target_'+instype] = True

def make_group_at(obj, block_count, classConstructor, prefix):
    assert type(obj) == asm_ast.Instruction
    parent = obj.parent
    # find obj in parents children
    try:
        i = parent.children.index(obj)
        if i == 0:
            if type(parent) == classConstructor:
                # it is in such a class
                return parent

        # split the parent's children
        first = parent.children[:i]
        second = parent.children[i:]

        # remove code from children of parent
        parent.children = first

        # create a block for second part (which contains obj)
        new_name = prefix + "_"+ str(block_count)
        new_block = classConstructor(new_name, second)

        # Now the tricky part, where in the tree should one add this block
        if type(parent) == classConstructor:
            # parent is of the same class
            # add in its parent, immediately after this
            parent.parent.add_child_after(new_block, parent)
            # block is attached, generate a new name
            new_block.gen_newname()
        else:
            # lets make a group of the previous children of the parent
            parent.children = []
            if len(first) > 0:
                extra_name = prefix+"x_" +str(block_count)
                extra_block = classConstructor(extra_name, first)
                parent.add_child(extra_block)
                # create a new name for the block
                extra_block.gen_newname()
            # Now add the child to the parent
            parent.add_child(new_block)
            # create a new name for the block
            new_block.gen_newname()

        return new_block
    except ValueError:
        logger.debug("Internal error - inconsistent AST; object not in its parents list")
        

def split_code_in_procedures (ast):
    # get visit each call target
    symtable = ast.symtable
    split_candidates = []
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Instruction:
            continue
        if 'target_call' in obj.properties:
            split_candidates.append(obj)

    # can't do this when enumerating the symtab
    # since the symtab is modified
    proc_count = 0
    for obj in split_candidates:
        make_group_at(obj, proc_count, asm_ast.Procedure, "proc")
        proc_count += 1

def wrap_code_in_blocks (ast):
    procedures = ast.descendants_of_type(asm_ast.Procedure)
    for proc in procedures:
        block_name = proc.name + "@entry"
        block_code = proc.children
        new_block = asm_ast.Block(block_name, block_code)
        proc.children = [new_block]
        new_block.parent = proc
        ast.symtable[new_block.name] = new_block


def split_code_in_blocks (ast):
    # get visit each call target
    symtable = ast.symtable
    candidates = []
    
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Instruction:
            continue
        elif ('target_jump' in obj.properties) or ('target_jumpif' in obj.properties):
            candidates.append(obj)

    # can't do this when enumerating symtab, since symtab
    # is modified
    block_count = 0
    for obj in candidates:
        make_group_at(obj, block_count, asm_ast.Block, "label")
        block_count += 1

def split_x (ast):
    # split blocks at at call, jump, jumpif,  ret
    symtable = ast.symtable
    candidates = []
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Instruction:
            continue
        instype = obj.instruction_type()
        if not instype in ['call', 'jump', 'ret', 'jumpif', 'error']:
            continue
        candidates.append(obj)

    block_count = 0
    for obj in candidates:    
        # find next sibling in the block
        succ = obj.next_sibling()
        if succ:
            make_group_at(succ, block_count, asm_ast.Block, "end")


def split_liveness (ast):
    # split blocks at 'mov eax,xxx' where xxx is an operand without register.
    symtable = ast.symtable
    candidates = []
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Block:
            continue
        #print " <<<<<<<< blocks for %s >>>>>>>>>" % key
        live_obj = liveness.Liveness(obj.children)
        live_obj.compute_liveness(set([]))
        for (block_type, instlist, live_regs) in live_obj.get_blocks():
            # print  instlist[0].address, block_type, len(instlist)
            # for inst in instlist:
            #     print inst
            block_start = symtable[instlist[0].address]
            assert(block_start.address == instlist[0].address)
            candidates.append((block_start, live_regs))
    block_count = 0
    for obj, live_regs in candidates:
        block = make_group_at(obj, block_count, asm_ast.Block, "liveness")
        block.properties['live_in'] = live_regs
        block_count +=1

                
def split_special_move (ast):
    # split blocks at 'mov eax,xxx' where xxx is an operand without register.
    symtable = ast.symtable
    candidates = []
    for key in symtable:
        obj = symtable[key]
        if not type(obj) == asm_ast.Instruction:
            continue
        defuse = liveness.get_defuse (obj)
        print obj
        print defuse
        
        if not obj.mnemonic == 'mov':
            continue
        if not len(obj.operands) == 2:
            continue
        op1 = obj.operands[0]
        op2 = obj.operands[1]
        if not type(op1) == asm_ast.RegisterOperand:
            continue
        if not op1.reg == "eax":
            continue
        if type(op2) == asm_ast.IndirectOperand \
                and (op2.segreg is not None or op2.reg1 is not None or op2.reg2 is not None):
            continue

        candidates.append(obj)

    block_count = 0
    for obj in candidates:    
            make_group_at(obj, block_count, asm_ast.Block, "special_move")

def relabel_using_target(ast, obj, parent_class):
    """obj must be a direct transfer instruction.
    Change its label using the name of the 'parent_class' of target instruction"""
    operand = obj.operands[0]
    symtable = ast.symtable
    if not type(operand) == asm_ast.ImmediateOperand:
        logger.debug("Error: relabel_using_target called with incorrect type")
        return None

    target_address = operand.value
    if not target_address in symtable:
        nearest_instruction = ast.get_containing_instruction(target_address)
        if nearest_instruction:
            nearest_address = nearest_instruction.address
            logger.debug("Error - Address %s is contained in Instruction at %s, referenced in %s" % (target_address, nearest_address, obj))
        else:
            logger.debug("Error - Address %s is outside program space" % target_address)
    else:
        target = symtable[target_address]
        # now lets get its parent
        parent = target.parent_of_type(parent_class)
        if not parent:
            logger.debug("Error -- relabel_using_target didn't find parent of right class")
            return None
        # Sanity check -- make sure the first instruction of parent is the same as target
        # which means a "JMP 0xABCD" should lead to a Block whose first instruction has address
        # 0xABCD. And a "CALL 0xABCD" should lead to a Procedure whose first instruction has
        # address 0xABCD. 
        checkobj = parent.first_child_of_type(type(target))
        if not checkobj == target:
            # There is an exception, though, for the instructions added
            # while refactoring the AST, for which we should not generate errors
            logging.debug("relabel_using_target: Consistency check failed on obj %s; parent is %s" % (obj, parent.name))
            return None
        # its all good, use the name of parent
        new_name = parent.name
        operand.label = new_name

def relabel_transfers(ast):
    """Change the labels for address of direct transfers generated by objdump.
    Use the newly generated names, instead."""
    transfers = get_direct_transfers(ast)
    for obj in transfers:
        transfer_type = obj.instruction_type()
        if transfer_type == 'call':
            # get the target address, then find its parent procedure
            # and use its name
            relabel_using_target(ast, obj, asm_ast.Procedure)
        if transfer_type in ['jump', 'jumpif']:
            # get the target instruction, find its parent Block
            # use its name
            relabel_using_target(ast, obj, asm_ast.Block)


