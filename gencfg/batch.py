import sys
import os
import fnmatch
import getopt
import re
import main
import logging

LOGFORMAT   = '%(asctime)-15s [%(name)s:%(filename)s:%(lineno)d]\t %(levelname)s %(message)s'
LOGLEVEL = logging.INFO
LOGFILE   = None
logger = logging.getLogger('binjuice')

message = """
     batch.py --srcdir= --destdir= --action=


        for each 'file' in srcdir, perform the following:
           main.py -i file -s : -u juice:semantics:types -g dot -o prefix/file
           default action = -u juice:all -g dot

           
      posible actions (from main.py):

      ## suboptions -- modify the output 
       -u|--unparse {prolog|abs|juice} (default prolog)   : type of unparsing
                juice may be further qualified to give the specific properties
                that should be output (see below).
       -m|--md5                  (default False)    : toggle, compute md5 hash after unparsing
       -l|--line-limit nn              (number of disassembled lines to process)
    
    ## primary options -- those that generate output
       -g|--graph {dot|csv}         : format of CFG graph
       -d|--disasm  filename        : output file for disassembly
       -i|--input   filename        : input file (disassembled)
       -j|--juice   filename        : juice file
"""

def find_files(directory, pattern, ignore_pattern):
    """Source: http://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python"""
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                result = re.search(ignore_pattern, filename)
                if not result:
                    yield filename


def usage():
    print message
    sys.exit(0)

LOGFILE = None
LOGLEVEL = "info"
action="-u juice:all -g dot"  #A edit, default is same

def process_options(argv):
    srcdir = None
    destdir = None
    global LOGFILE
    global LOGLEVEL
    
    try:
        opts, args = getopt.getopt(argv, "h", ["help", "srcdir=", "destdir=", "loglevel=", "logfile=", "action="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("--srcdir"):
            srcdir = a
        elif o in ("--destdir"):
            destdir = a
        elif o in ("--loglevel"):
        	LOGLEVEL = a
        elif o in ("--logfile"):
        	LOGFILE = a
        elif o in ("--action"):
        	action = a
        else:
            print "ERROR: Unhandled option '%s'" % o
            usage()

    if not srcdir or not destdir:
        print "ERROR: Insufficient arguments, must provide --srcdir= and --destdir="
        sys.exit(0)

    return (srcdir, destdir)


def make_cmd(input_file, output_file):
                action_pattern = action
                output_pattern = "-o %s" #changed A
                #input_pattern  = "-l 500000 -i %s"
                input_pattern  = "-i %s"
                juice_pattern  = "-s :"

                #print output_file
                cmd = output_pattern % output_file
		cmd += " " + action_pattern
		cmd += " " + input_pattern % input_file
		cmd += " " + juice_pattern
		if LOGLEVEL:
			cmd += " --loglevel %s" % LOGLEVEL
		if LOGFILE:
			cmd += " --logfile %s" % LOGFILE
		print cmd
		return cmd
	

def do_main(args):
    (srcdir, destdir) = process_options(args)
    if srcdir.endswith(os.sep):
        srcdir = srcdir[:-1]
    if destdir.endswith(os.sep):
        destdir = destdir[:-1]
   
    for input_file in find_files(srcdir, "*", r"(\.svn)|(\.log)|(agent)|(banker)"):
    	# The "+1, is to skip '/' as well"
    	suffix = input_file[len(srcdir)+1:]
    	output_file = os.path.join(destdir, suffix)
    	if os.path.exists(output_file):
    		# ignore, if the file already exists
    		print("Skipping %s as %s exists" % (input_file, output_file))
    		logger.info("Skipping %s as %s exists" % (input_file, output_file))
    	else:
    		cmd = make_cmd(input_file, output_file)
    		try:
    			main.do_main(cmd.split())
    		except:
    			print "Error processing %s, exception %s " % (input_file, sys.exc_info()[0])
    		

    		

if __name__ == "__main__":
    do_main(sys.argv[1:])

# sample
#     F:
#     cd Arun\Projects
#     python  workspace-collection\maagi\binjuice\src\batch.py --srcdir=f:\temp\maagi\1-objdump --destdir=f:\temp\maagi\3-juice2 --logfile f:\temp\maagi\juice.log
