#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1802 $
#   $Author: arun $
#   $Date: 2012-08-23 17:45:31 -0500 (Thu, 23 Aug 2012) $

#  This file implements a parser for SRL's trustdis disassembly output.

import re
import asm_ast
import instruction_parser

import logging
logger = logging.getLogger('trustdis_parser')

# Python iterators do not provide capability for "pushback".
# That makes it hard to write a parser, since most parser require lookahead
# The pushback.py program found on the Internet provides the capability to
# push a value back on an iterator.
import pushback

#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# The trustdis syntax is described in the document trustdis-syntax.txt
# The syntax was 'reverse engineered' by looking at its output. Its likely
# that the parser does not exercise all the capabilities, and I missed some issues.

# The following regular expressions give rules for parsing specific types of lines
# They are used within the parser


#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# Some utility functions

def trustdis_chomp(s):
    """Taken from http://stackoverflow.com/questions/3849509/python-how-to-remove-n-from-a-list-element"""
    """Remove newline at the end of a line"""
    try:
        s = s[:s.index(';')].rstrip()
    except ValueError:
        pass
    
    if s.endswith('\n'):
        return s[:-1]
    else:
        return s


def skip_empty_lines(i):
    "Skip 0 or more empty lines. Put the non empty line back"
    while (1):
        (num,str) = i.next()
        if not str == "":
            i.pushback((num,str))
            return


        
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

class Parser(object):
    """Abstract class, parser, unparser, for program"""
    def __init__ (this):
        this.instruction_parser = instruction_parser.Parser()
        
    def parse_file(this, filename):
        """Toplevel function for parsing objdump generated file. It doesn't currently check if the
        file exists. The caller needs to handle exception if the file is not found."""

        lines = [trustdis_chomp(line) for line in open(filename)]
        if len(lines) == 0:
            logger.debug("File %s is empty" % filename)
            return Program('none', None, [])
        i = pushback.pushback_wrapper(iter(enumerate(lines)))
        return this.program(i)

    def program (this, itr):
        instructions = []
        while (1):
            try:
                new_instruction = this.instruction_parser.parse(itr)
            except StopIteration:
                break
            if not new_instruction:
                break
            instructions.append(new_instruction)
        return asm_ast.Program('noname', 'noformat', instructions)
