#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1961 $
#   $Author: aaron $
#   $Date: 2013-02-06 16:51:29 -0600 (Wed, 06 Feb 2013) $

import re
import asm_ast

import logging
logger = logging.getLogger('parser')

re_qualifier = re.compile(r"(?P<qual>(rep)|(repz)|(repnz)|(repne)|(repe)|(addr16)|(addr32)|(data16)|(data32)|(lock))\s")

re_instruction = re.compile(r"""
  \s* (?P<address>[0-9a-fA-F]+):       # Address of instruction
  \s+ (?P<code> ([0-9a-fA-F][0-9a-fA-F]\s)+)  # binary code
  \s* (?P<mnemonic>(([a-zA-Z][a-zA-Z0-9]*)|(\(bad\))))?     # mnemonic is optional, since objdump skips it sometimes
  \s*(?P<args>.*?)$                    # arguments
 """, re.VERBOSE)  ##Changed + to * on the first \s

re_ellipse = re.compile(r"\s*\.\.\.")

# indirect1 ::= {<ptr> PTR} {<segreg>:} [ <ind_arg> ]
re_indirect1 = re.compile(r"""
   ((?P<ptr>(fword)|(qword)|(byte)|(word)|(dword))\s (ptr\s)? )?  # prefix
   ((?P<segreg>[a-z]+):)? \[(?P<ind_arg>.*) \]
   """, re.VERBOSE)

re_indirect2 = re.compile(r"""
   ((?P<ptr>(fword)|(qword)|(byte)|(word)|(dword))\s (ptr\s)? )?  # prefix
   (?P<segreg>[a-z]+): (?P<disp>.*)
   """, re.VERBOSE)


# <ind_arg> ::= {<reg1>} {+ <reg2> {* <xplier>}}  {(+|-) <disp>}
# <ind_arg> ::=           <reg2> {* <xplier>}  {(+|-) <disp>}

re_ind_arg1 = re.compile(r"""
   (?P<reg1> ([a-z]+|r[0-9]+[a-z]?))
   (\+ ((?P<reg2> ([a-z]+|r[0-9]+[a-z]?)) (\* (?P<xplier> [1-8]) )?))?
   (?P<disp> [\+\-] (0x)? [0-9a-fA-F]+)?$
   """, re.VERBOSE)

re_ind_arg2 = re.compile(r"""
   ((?P<reg2> ([a-z]+|r[0-9]+[a-z]?)) (\* (?P<xplier> [1-8]) )?)
   (?P<disp> [\+\-]? (0x)? [0-9a-fA-F]+)?$
   """, re.VERBOSE)

# direct operand: special case - call   402e2c <_IO_putc@plt+0x3c>
re_direct_special = re.compile(r"(?P<hex>[0-9a-fA-F]+)\s\<(?P<label>.+)\>")

# direct operand: normal case:
#          register | num | 0xnn | 0xnn:nnnn

re_direct = re.compile(r"""
   (?P<reg> ([a-z]+|r[0-9]+[a-z]?))$
   | (?P<num> -? [0-9a-fA-F]+)$
   | (?P<hex> 0x[0-9a-fA-F]+)$
   | (?P<off> 0x[0-9a-fA-F]+):(?P<disp>.*)$

    """, re.VERBOSE)

class Parser(object):
    def parse (this, itr):
        (lineno, line) = itr.next()
        # force all lower case, to support both objdump and trustdis
        # but save the original, in case you need to push it back
        orig_line = line
        line = line.lower()
        # check if instruction has 'rep' or 'addr16'
        # remove it, and remember
        result = re_qualifier.search(line)
        qual = None
        if result:
            qual = result.group('qual')
            line = re_qualifier.sub("", line)

        result = re_instruction.match(line)

        if not result:
            result = re_ellipse.match(line)
            if not result:
                itr.pushback((lineno, orig_line))
                return None
            # skip ellipses, and read the next line
            return this.parse(itr)
        
        address = result.group('address')
        # patch 0x, since its missing in the disassembly
        address = "0x" + address
        code = result.group('code')
        # convert code to list
        code = code.split(' ')
        # Since ' ' terminates the byte code list, remove the last entry
        if code[-1] == '':
        	code.pop(-1)
        mnemonic = result.group('mnemonic')
        # clean up mnemonic when (bad) or empty
        if not mnemonic:
            # this means, the byte code is continuing previous instruction
            mnemonic = "continue"

        rest = result.group('args')
        operands = []
        if rest:
            arglist = rest.split(",")
            for arg in arglist:
                # clean up spaces; trustdis has spaces after comma; objdump doesn't
                arg = arg.strip()
                for parse_op in [this.direct_special_operand, this.direct_operand, this.indirect_operand]:
                    op = parse_op(arg)
                    if op:
                        break
                if not op:
                    logger.debug("Syntax error - unparsable operand %s at line %d\n  in \"%s\"" % (arg,lineno, line))
                else:
                    operands.append(op)
        # bump lineno to be consistent with editors. The lineno is really an index starting at 0
        # whereas line numbers start at 1
        instr = asm_ast.Instruction(lineno+1, address, code, qual, mnemonic, operands) 
        return instr

    def direct_special_operand (this, str):
        result = re_direct_special.match(str)
        if not result:
            return None
        hex = result.group('hex')
        # patch the hex -- objdump leaves out the 0x prefix
        hex = "0x" + hex
        label = result.group('label')
        op = asm_ast.ImmediateOperand(hex, label)
        return op
        
    def direct_operand (this, str):
        result = re_direct.match(str)
        if not result:
            return None
        reg = result.group('reg')
        num = result.group('num')
        hex = result.group('hex')
        off = result.group('off')
        disp = result.group('disp')
        op = None
        if reg:
            op = asm_ast.RegisterOperand(reg)
        elif num:
            op = asm_ast.ImmediateOperand(num)
        elif hex:
            op = asm_ast.ImmediateOperand(hex)
        elif off:
            op = asm_ast.ImmediateOffset(off, disp)
        return op

    def indirect_operand (this, str):
        result = re_indirect1.match(str)
        ptr = None
        segreg = None
        reg1 = None
        reg2 = None
        xplier = None
        disp = None
        optype = 0
        if result:
            optype = 1
            ptr = result.group('ptr')
            segreg = result.group('segreg')
            ind_arg = result.group('ind_arg')

            result = re_ind_arg1.match(ind_arg)
            if result:
                optype = 2
                reg1 = result.group('reg1')
                reg2 = result.group('reg2')
                xplier = result.group('xplier')
                disp = result.group('disp')
            else:
                result = re_ind_arg2.match(ind_arg)
                optype = 3
                if result:
                    optype = 4
                    reg1 = None
                    reg2 = result.group('reg2')
                    xplier = result.group('xplier')
                    disp = result.group('disp')

        else:
            result = re_indirect2.match(str)
            optype = 5
            if result:
                optype = 6
                ptr  = result.group('ptr')
                segreg = result.group('segreg')
                reg1 = None
                reg2 = None
                xplier = None
                disp = result.group('disp')
        # Sanity check
        # [reg1 + reg2*xplier + disp]
        # at least one og reg1, reg2, or disp should be non-None
        if not (reg1 or reg2 or disp):
            logger.debug("Syntax error: optype = %d" % optype)
            return None
        # if xplier is set, then reg2 should be set too
        if xplier and (not reg2):
            logger.debug("Syntax error: xplier = %s and reg2 = %s" % (xplier, reg2))
            return None

        return asm_ast.IndirectOperand(ptr, segreg, reg1, reg2, xplier, disp)


