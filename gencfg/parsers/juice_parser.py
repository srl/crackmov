#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1802 $
#   $Author: arun $
#   $Date: 2012-08-23 17:45:31 -0500 (Thu, 23 Aug 2012) $

"""Syntax of Juice

Example:
    Block: _text@0x10280@0xe8
            Code
                    add(A,B)
                    cmp(C,D)
                    jne
            Semantics
                    A=B+def(A)
                    E=cmp(def(C),D)
            Simplifications
            Types
                    type(A)=reg(sp)
                    type(B)=int
                    type(C)=reg(gp)
                    type(D)=int
                    type(E)=reg(flag)

"""

import pushback
import re
import asm_ast

import logging
logger = logging.getLogger('juice_parser')

def chomp(s):
    return s.rstrip('\r\n')

class Parser (object):
    """Parses a juice file created by the Prolog binjuicer"""
    
    def __init__ (this, asm_ast):
        # parse juice extracted by Prolog
        # and attach to corresponding node in the ast
        this.ast = asm_ast
        this.selected_block = None

    def select_block (this, blockname):
        if not this.ast:
            return None
        
        symtab = this.ast.symtable
        if not blockname in symtab:
            logger.debug("Syntax error: Ast does not have a block with name %s" % blockname)
            this.selected_block = None
        else:
            this.selected_block = symtab[blockname]
        return this.selected_block

    def update_property (this, property_name, property_value):
        property_name = property_name.lower()
        if not this.selected_block:
            return
        this.selected_block.properties[property_name] = property_value
        
    def parse_file (this, filename):
        lines = [chomp(line) for line in open(filename)]
        if len(lines) == 0:
            logger.debug("File %s is empty" % filename)
            return Program('none', None, [])
        i = pushback.pushback_wrapper(iter(enumerate(lines)))
        return this.parse_program(i)

    def parse_program (this, itr):
        """ Parse a program, which is a sequence of Blocks"""
        try:
            while (1):
                this.parse_block(itr)
        except StopIteration:
            pass
        
    def parse_block (this, itr):
        """Parse a block, consisting of
           a block header, followed by properties"""
        (lineno, firstline) = itr.next()
        if firstline == "":
            return None
        result = re.match(r"ERROR:", firstline)
        if result:
            return None
        result = re.match(r"Block: (?P<name>.*)$", firstline)
        if not result:
            logger.error("Syntax Error: Incorrect block header line at %d" % lineno)
            return None
        
        blockname = result.group('name')
        this.select_block (blockname)
        
        property_list = []
        while (1):
            try:
                property = this.parse_property(itr)
            except StopIteration:
                break
            if not property:
                break
            property_list.append(property)
        return (blockname, property_list)

    def parse_property (this, itr):
        """Parse a property, consisting of a property header line
           followed by property lines.
           A property header line is a tab followed by a name."""
        (lineno, firstline) = itr.next()
        result = re.match(r"\t(?P<name>[a-zA-z0-9]*)", firstline)
        if not result:
            itr.pushback((lineno, firstline))
            return None
        property_name = result.group('name')
        value_list = []
        while 1:
            try:
                value = this.parse_value(itr)
            except StopIteration:
                break
            if not value:
                break
            value_list.append(value)
        this.update_property(property_name, value_list)
        return (property_name, value_list)

    def parse_value (this, itr):
        """Parse a value line. A value line is three tabs followed by text"""
        (lineno, firstline) = itr.next()
        result = re.match(r"\t\t(?P<value>[a-zA-z0-9].*)", firstline)
        if not result:
        	itr.pushback((lineno, firstline))
        	return None
        return result.group('value')
    
