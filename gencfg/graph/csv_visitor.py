#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1809 $
#   $Author: arun $
#   $Date: 2012-08-25 14:14:11 -0500 (Sat, 25 Aug 2012) $

import csv
import graph

class Annotator (object):
    def node_label (obj):
        return None
    
# print graph in CSV format (for gephi)
class GraphOutput (graph.GraphVisitor):
    def __init__(self, csv_notator):
        super(type(self), self).__init__()
        self.csv_notator = csv_notator
        self.file_ext = "csv"
        
    def edge(self, edge):
        if self.csv_notator:
            src_label = self.csv_notator.node_label(edge.src)
            dest_label = self.csv_notator.node_label(edge.dest)
        else:
            src_label = "%s" % edge.src
            dest_label = "%s" % edge.dest
            
        print "\"%s\",\"%s\"" % (src_label, dest_label)
