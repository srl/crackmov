#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2467 $
#   $Author: arun $
#   $Date: 2015-06-21 18:16:51 -0500 (Sun, 21 Jun 2015) $
import os
import sys

# class to represent API nodes
class GraphNode (object):
    def __init__(self, obj, parent, position=0):
        # obj is whatever object is used to create the graph
        self.obj = obj
        self.inedges = []
        self.outedges = []
        self.parent = parent
        self.position = position
        
    def id(self):
        return self.obj.name
    
    # add a new edge, or update count of existing edge
    # if the dest id is the same its counted as same edge
    def add_edge_to(self, to_node):
        for edge in self.outedges:
            # check if edge already exists
            if edge.dest.id() == to_node.id():
                edge.update(to_node)
                return edge
        new_edge = Edge(self, to_node)
        self.outedges.append(new_edge)
        to_node.inedges.append(new_edge)
        return new_edge


# to represent edges
class Edge (object):
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def update (this, dest):
        """Edge exists; this method is to update if a new edge
        with same destination is created"""
        pass

# to represent graphs
class DirectedGraph (object):
    def __init__(self, obj, type):
        self.obj = obj
        self.node_dict = {}
        self.type = type
        self.node_count = 0
        if obj.name:
            self.name = obj.name
        
    def add_node(self, obj):
        if not obj in self.node_dict:
            self.node_dict[obj] = GraphNode(obj, self, self.node_count)
            self.node_count += 1
        return self.node_dict[obj]

    def find_node(self, obj):
        if obj in self.node_dict:
            return self.node_dict
        return None
    

    def num_edges(self):
        count = 0
        for obj in self.node_dict:
            node = self.node_dict[obj]
            count += len(node.outedges)
        return count
    
    def visit(self, visitor):
        visitor.graph_start(self)

        # visit nodes
        visitor.node_start(len(self.node_dict.keys()))
        for node in sorted(self.node_dict.values(), key=lambda x: x.position):
            visitor.node(node)
        visitor.node_end()

        # visit edges
        visitor.edge_start(self.num_edges())
        for key in self.node_dict.keys():
            node = self.node_dict[key]
            for edge in node.outedges:
                visitor.edge(edge)
        visitor.edge_end()

        # end graph
        visitor.graph_end()



class GraphVisitor(object):
    def __init__(self):
        self.save_stdout = None
        self.outstream = None
        self.file_ext = None
    def open (self, outdir, filename):
        self.save_stdout = sys.stdout
        # ensure that the output directory exists
        if not os.path.exists(outdir):
        	os.makedirs(outdir)
        outfile = os.path.join(outdir, filename)
        self.outstream = open(outfile, 'w')
        sys.stdout = self.outstream

    def close (self):
        if self.outstream:
            self.outstream.close()
        if self.save_stdout:
            sys.stdout = self.save_stdout
            
    def graph_start(self, graph):
        return True
    def node_start(self, count):
        return True
    def node(self, node):
        return True
    def node_end(self):
        return True
    def edge_start(self, count):
        return True
    def edge(self, edge):
        return True
    def edge_end(self):
        return True
    def graph_end(self):
        return True
