#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2470 $
#   $Author: arun $
#   $Date: 2015-06-25 22:03:58 -0500 (Thu, 25 Jun 2015) $

import graph
import unparse2prolog

# print graph in Prolog format
class GraphOutput (graph.GraphVisitor):
    def __init__(self, unparser):
        super(type(self), self).__init__()
        self.unparser = unparser
        self.file_ext = "pro"
        
    def get_id(self, obj):
        if obj.id:
            return obj.id()
        return None
    
    def get_annotation(self, obj):
        return None
    
    def graph_start(self, graph):
        return True

    def node(self, node):
        id = self.get_id(node)
        annot = self.unparser.unparse(node.obj)
        if annot.strip() == "": annot = "[]"
        result = "node('%s', %s).\n" % (id, annot)
        print result
        return True
    
    def edge(self, edge):
        src_id = self.get_id(edge.src)
        dest_id = self.get_id(edge.dest)
        result = "edge('%s', '%s').\n" % (src_id, dest_id)
        print result
        return True

    def graph_end(self):
        return True

