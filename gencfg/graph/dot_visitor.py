#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1809 $
#   $Author: arun $
#   $Date: 2012-08-25 14:14:11 -0500 (Sat, 25 Aug 2012) $

import graph

class Annotator (object):
    def id(this, obj):
        return None
    def annot(this, obj):
        return None


# print graph in Dot format (for graphviz)
class GraphOutput (graph.GraphVisitor):
    def __init__(self, dotator):
        super(type(self), self).__init__()
        self.file_ext = "dot"
        self.dotator = dotator

    def get_id(self, obj):
        if self.dotator.id:
            return self.dotator.id(obj)
        if obj.id:
            return obj.id()
        return None
    
    def get_annotation(self, obj):
        if self.dotator.annot:
            return self.dotator.annot(obj)
        return None
    
    def graph_start(self, graph):
        print "digraph %s {" % self.get_id(graph)
        return True

    def node(self, node):
        id = self.get_id(node)
        annot = self.get_annotation(node)
        result = id
        if annot:
            result += " " + annot
        print result
        return True
    
    def edge(self, edge):
        src_id = self.get_id(edge.src)
        dest_id = self.get_id(edge.dest)
        
        annot = self.get_annotation(edge)
        result = "\t%s -> %s" % (src_id, dest_id)
        if annot:
            result += " " + annot
        print result
        return True

    def graph_end(self):
        print "}"
        return True

