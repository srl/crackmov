#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2485 $
#   $Author: arun $
#   $Date: 2015-07-06 17:53:35 -0500 (Mon, 06 Jul 2015) $

import sys
import os
import getopt
import re
import logging

LOGFORMAT   = '%(asctime)-15s [%(name)s:%(filename)s:%(lineno)d]\t %(levelname)s %(message)s'
LOGLEVEL = logging.INFO
LOGFILE   = None
logger = logging.getLogger('binjuice')

def update_path (subdir):
    libpath = os.path.join(os.path.dirname( __file__), subdir)
    if not libpath in sys.path:
        sys.path.append(libpath)

update_path("ast")
update_path("parsers")
update_path("unparser")
update_path("graph")
update_path("cfg")

import asm_ast
import objdump_parser
import trustdis_parser
import juice_parser
import unparse2prolog
import unparse2abs
import unparse2md5
import unparse2juice

import restructure_ast

import cfg
import dot_visitor
import cfg2dot
import csv_visitor
import cfg2csv
import prolog_visitor
try:
    import prolog_server
    json_available = True
except ImportError:
    json_available = False

message = """main.py  -h -o outdir -u (prolog|abs|juice(:property)*) -m -g (dot|csv|prolog) -d outfile -m -j juicefile -l nn -i filename
    ## suboptions -- modify the output 
       -o|--outdir  outdir       (default .)        : output directory
       -u|--unparse {prolog|abs|juice} (default prolog)   : type of unparsing
                juice may be further qualified to give the specific properties
                that should be output (see below).
       -m|--md5                  (default False)    : toggle, compute md5 hash after unparsing
       -l|--line-limit nn              (number of disassembled lines to process)
    
    ## primary options -- those that generate output
       -g|--graph {dot|csv}         : format of CFG graph
       -d|--disasm  filename        : output file for disassembly
       -i|--input   filename        : input file (disassembled)
       -j|--juice   filename        : juice file
       -s|--juice-server host:port  : juice server
       
    ## for debugging
       --logfile     filename       : file for logging
       --loglevel    level          : level of log, one of (debug,info,warn,error,critical)
       -h|--help                    : this message
    # NOTE --
     1) Options may be repeated.
     2) Action of the primary options is qualified by the suboptions preceding it.
     3) Each suboption overrides previous occurrence of that option (or default).
     Example:
         python main.py -o dir1 -u prolog -g dot \
                        -o dir2 -u abs -m -g csv \
                        -u prolog -m -d disasm1 \
                        -u abs -d disasm2
         will generate four different outputs:
             - CFG in dot format with block in prolog in dir1
             - CFG in csv format with md5 of abstracted code in block in dir2
             - disassembly in prolog format in file disasm1
             - disassemgly in abs format in file disasm2
     4) When outputing (unparsing) 'juice', you can specify which properties to include as follows:
           -u juice:semantics:code:simplifications:types
           if no property is specified, a default set of properties is used.
"""


def usage():
    print message
    sys.exit(0)


class OutputOption (object):
    def __init__ (this, unparse_type, do_md5):
        this.unparse_type = unparse_type
        this.do_md5 = do_md5

        if this.unparse_type == "prolog":
            this.unparser = unparse2prolog.Unparser(show_code=False, show_lineno = True, show_address = True)
        elif this.unparse_type == "full":
            this.unparser = unparse2prolog.Unparser(show_code=True, show_lineno = True, show_address = True)
        elif this.unparse_type == "abs":
            this.unparser = unparse2abs.Unparser()
        elif re.match(r"juice", this.unparse_type):
        	# get the properties, if specified
        	# Syntax: juice:semantics:types
        	properties = this.unparse_type.split(':')[1:]
        	if len(properties) == 1 and properties[0] == "all":
        		properties = unparse2juice.JUICE_PROPERTIES
        	if len(properties) == 0:
        		properties = ['semantics', 'types']
        	# create a map of unparsers, one per property
        	# this is useful when creating dot files. 
        	# each property is shown as a separate label
        	if len(properties) == 1:
        	    this.unparser = unparse2juice.Unparser(properties[0])
        	else:
        		this.unparser = {}
        		for property in properties:
        			if not property in unparse2juice.JUICE_PROPERTIES:
        				print "ERROR: %s is not a valid juice property %" % property
        				sys.exit(0)
        			this.unparser[property] = unparse2juice.Unparser(property)
        else:
            print "ERROR: Unrecognized unparser type '%s'" % unparser_type
            usage()

        # check if md5 processing is requested
        if this.do_md5:
            # md5 is requested
            # and pass the previous unparser
            this.unparser = unparse2md5.Unparser(this.unparser)

    def process (this, ast, cfg_collection):
  	    pass

class CfgOutputOption (OutputOption):
    def __init__ (this, outdir, unparse_type, graph_type, do_md5 = False):
        super(type(this), this).__init__(unparse_type, do_md5)
        this.outdir = outdir
        this.graph_type = graph_type
        if this.graph_type == "dot":
            this.cfg_visitor = dot_visitor.GraphOutput(cfg2dot.Annotator(this.unparser))
        elif this.graph_type == "csv":
            this.cfg_visitor = csv_visitor.GraphOutput(cfg2csv.Annotator(this.unparser, True))
        elif this.graph_type == "prolog":
            this.cfg_visitor = prolog_visitor.GraphOutput(this.unparser)
        else:
            print "ERROR: Unrecognized graph type '%s'" % graph_type
            usage()

    def process (this, ast, cfg_collection):
        if not os.path.exists(this.outdir):
            os.makedirs(this.outdir)

        cfg_cache = cfg_collection.get_cfgs()
        logger.info("Saving CFG in %s format to directory '%s'" % (this.graph_type, this.outdir))
        for a_cfg in cfg_cache:
        	out_file = a_cfg.name
        	# create file name, with appropriate extension
        	if this.cfg_visitor.file_ext:
        		out_file += "." + this.cfg_visitor.file_ext
        	this.cfg_visitor.open(this.outdir, out_file)
        	a_cfg.visit(this.cfg_visitor)
        	this.cfg_visitor.close()

class DisasmOutputOption (OutputOption):
    def __init__ (this, output_file, outdir, unparse_type, do_md5):
        super(type(this), this).__init__(unparse_type, do_md5)
        this.output_file = output_file
        this.outdir = outdir

    def process (this, ast, cfg_collection):
        logger.info("Writing disassembly to %s" % this.output_file)

        result = this.unparser.unparse(ast)
        fp = open(this.output_file, "w")
        fp.write(result)
        fp.close()
try:
    import hashlib
except:
    pass

def compute_hash(msg):
	m = hashlib.md5()
	m.update('%r'%msg)
	return m.hexdigest()

class JuiceMap (object):
	def __init__ (self):
		self.property_map = {}
                self.block_map = set()
                self.hash_property = None

        def compute_juice_map (self, ast, property):
            self.hash_property = property
            for block in ast.descendants_of_type(asm_ast.Block):
                value = block.properties.get(property, None)
                if value is not None:
                    self.add(property, value, block)
            return self

	def add (self, property, value, obj):
		if not property in self.property_map.keys():
			self.property_map[property] = {}
		p = self.property_map[property]
		h = compute_hash(value)
		if not h in p.keys():
			p[h] = []
		p[h].append(obj)
                self.block_map |= set([obj])
		return self
	def fetch (self, property):
		return self.property_map.get(property, {})
        def report (self, property):
            p = self.fetch(property)
            hash_count = 0
            block_count = 0
            hist = {}
            for h in sorted(p.keys()):
                block = sorted(p[h], key=lambda x: int(x.start_address(), 16))[0]
                address = block.start_address()
                code = block.properties.get('code', "code_undefined")
                value = block.properties.get(property, "prop_undefined")
                simp = block.properties.get('gen_types', "simp_undefined")

                nblocks = len(p[h])
                print "----------  %d copies ---------" % nblocks
                print 'code: ', address,code
                print 'value:', value
                print 'types:', simp
                print "----------"

                hist[nblocks] = hist.get(nblocks, []) + [len(code)]
                hash_count +=1 
                block_count += nblocks
            for nblocks in sorted(hist.keys()):
                print "%d hashes have %d clones of sizes %r" % (len(hist[nblocks]), nblocks, sorted(hist[nblocks]))

            print "Hashes %r: %d, blocks %d" % (self.hash_property, hash_count, block_count)

class InputOption (object):
    def __init__ (this, input_file, juice_file, juice_server, line_limit):
        this.input_file = input_file
        this.line_limit = line_limit
        this.juice_file = juice_file
        if juice_server and (not json_available):
            print("ERROR: Sorry, json library needed for prolog server are not available")
            print "  use precomputed juice instead"
            sys,exit(0)
        this.juice_server = juice_server
        if this.juice_server and this.juice_file:
        	# cannot get juice from both, the server and file
        	print "ERROR: Only one of juice server or juice file may be selected"
        	sys.exit(0)
        
        
    def process (this):         
    	if not os.path.exists(this.input_file):
            logger.error("file \"%s\" does not exist" % this.input_file)
            print "Error: file \"%s\" does not exist" % this.input_file
            sys.exit(1)

   	    # setup unparser used for converting to strings (for messages)
    	default_unparser  = unparse2prolog.Unparser(show_code=False, show_lineno=False, show_address=True)
    	asm_ast.AST.unparser = default_unparser
    	global selected_parser
   	    # parse the input file
    	if not selected_parser:
            # choose a default parser
            select_parser("objdump")

        logger.info("Parsing file %s" % this.input_file)
       # logger.info("With Parser %s" % selected_parser)   #debugging change 1- A
       # logger.info("With Line limit %s" % this.line_limit)   #debugging change 1- A
    	ast = selected_parser.parse_file(this.input_file, this.line_limit)
    	# restructure the AST, to create procedures and blocks, and link
    	# references to declarations
    	
    	if not ast:
            logger.error("Parsing error, AST not created")
    	else:
            inst_count = len(ast.symtable)
            logger.info("Parsed file with %d instructions" % inst_count)
            logger.info("Restructuring ast")
            restructure_ast.restructure(ast)
    
    	if this.juice_file:
    		logger.info("Processing juice file")
    		juice_parser.Parser(ast).parse_file(this.juice_file)
        elif this.juice_server:
        	logger.info("Querying prolog server for juice")
        	server = prolog_server.Server(this.juice_server)
        	server.juice_program(ast)
                logger.info("Finished juicing")
                logger.info("Computing map")
                juice_map = JuiceMap()
                juice_map.compute_juice_map (ast, 'gen_code')
                juice_map.report('gen_code')
                
        return ast
	    
#'''''''''''''''''''''''''''''''''''''''''''
        
selected_parser = None

def select_parser (parser_type):
    global selected_parser
    if parser_type == "objdump":
        selected_parser = objdump_parser.Parser()
    elif parser_type == "trustdis":
        selected_parser = trustdis_parser.Parser()
    else:
        print "ERROR: Unrecognized parser type '%s', choose one of 'objdump' or 'trustdis'" % parser_type
        usage()
    return selected_parser

def process_options(argv):
    # set up default values
    global selected_parser
    selected_parser = None
    
    unparse_type = "prolog"
    graph_type   = "prolog"
    do_md5       = False
    outdir       = "."
    input_file   = None
    juice_file   = None
    juice_server = None
    line_limit   = None
    
    global LOGFILE
    global LOGLEVEL
    result = []
    try:
        opts, args = getopt.getopt(
        	            argv, 
        	            "hd:g:u:mo:p:i:j:s:l:", 
        	            ["help", "disasm=", "graph=",  "unparse=", "md5", "outdir=", "parser=", "input=", 
                         "juice=", "juice-server=", "line-limit=", "logfile=", "loglevel="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-o", "--outdir"):
            outdir = a
        elif o in ("-u", "--unparse"):
            unparse_type = a
        elif o in ("-m", "--md5"):
            do_md5 = not do_md5
        elif o in ("-g", "--graph"):
            graph_type = a
            result.append(CfgOutputOption(outdir, unparse_type, 
                                          graph_type, do_md5))
        elif o in ("-d", "--disasm"):
            output_file = a
            result.append(DisasmOutputOption(output_file, outdir, unparse_type, do_md5))
        elif o in ("-p", "--parser"):
            select_parser(a)
        elif o in ("-i", "--input"):
            input_file = a
        elif o in ("-j", "--juice"):
            juice_file = a
        elif o in ("-s", "--juice-server"):
            juice_server = a
        elif o in ("-l", "--line-limit"):
            line_limit = int(a)
        elif o == "--logfile":
        	LOGFILE = a
        elif o == "--loglevel":
        	level_map = {'debug':logging.DEBUG, 'info':logging.INFO, 'warn':logging.WARN, 
        	             'error':logging.WARN, 'critical':logging.CRITICAL}
        	a = a.lower()
        	if not a in level_map:
        		print "INCORRECT log level, use one of (debug, info, warn, error, critical)"
        		sys.exit(0)
        	LOGLEVEL = level_map[a]
        else:
            print "ERROR: Unhandled option '%s'" % o
            usage()
    
    # now all the args should have been processed
    if not len(args) == 0:
        print "ERROR: Too many or incorrect arguments"
        print "   To get help, use -h or --help"
        sys.exit(0)
    elif not input_file:
        print "ERROR: No input file selected. There is nothing to do"
        print "   To get help, use -h or --help"
        sys.exit(0)
    elif len(result) == 0:
        print "No action requested, will simply parse the given file"

    # put the input file to process as the first argument
    result.insert(0, InputOption(input_file, juice_file, juice_server, line_limit))
    return result                                            


def set_console_logger():
	# http://docs.python.org/release/2.5/lib/multiple-destinations.html
	# define a Handler which writes INFO messages or higher to the sys.stderr
	console = logging.StreamHandler()
	# CAUTION: The logging level for console is the higher of
	#  that set here and the one set in basicConfig.
	console.setLevel(logging.WARN)
	# set a format which is simpler for console use
	formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
	# tell the handler to use this format
	console.setFormatter(formatter)
	# add the handler to the root logger
	logging.getLogger('').addHandler(console)

def do_main(args):
    opts = process_options(args)
    # Logging levels DEBUG, INFO, WARNING, ERROR, CRITICAL
    #   basicConfig has to be done, even if LOGFILE is None.
    #   and if LOFILE is None, the log is directed to console
    logging.basicConfig(format=LOGFORMAT, level=LOGLEVEL, filename=LOGFILE)
    # Also have the log go console, using method described in the following URL.
    set_console_logger()
    
    logger.info("Start new processing")
    # error check already done by process_options
    try:
        logger.debug("Param Options: %r" % opts)
    	input_options = opts[0]
    	ast = input_options.process()
        if not ast: return
    	cfg_collection = cfg.CfgCollection(ast)
    	for opt in opts[1:]:
            opt.process(ast, cfg_collection)
    except: # trap all errors
        import traceback
    	e = traceback.format_exc()
    	logger.error("Processing terminated due to error %s" % e)
        raise
    logger.info("Completed")
    logging.shutdown()
    
if __name__ == "__main__":
    do_main(sys.argv[1:])

#'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
# test
# python main.py -o ../cfg.dot -u prolog -g dot -o ../cfg.abs -u abs -g dot -o ../cfg.abs.md5 -m -g csv -m -d ../disasm.abs -u prolog -d ../disasm.prolog
