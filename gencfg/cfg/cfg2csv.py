#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1886 $
#   $Author: arun $
#   $Date: 2012-10-04 13:56:40 -0500 (Thu, 04 Oct 2012) $

import graph
import csv_visitor
import asm_ast

class Annotator(csv_visitor.Annotator):
    def __init__ (this, unparser, use_block_content = False):
        super(type(this), this).__init__()
        this.unparser = unparser
        
        # when generating labels, should be use
        # the content of the block, or its name
        # default is to use the name
        this.use_block_content = use_block_content

    def unparse (this, obj):
        if this.unparser:
            return this.unparser.unparse(obj)

        # else use default
        return "%s" % obj

    def node_label (this, obj):
        label = None
        if type(obj) == graph.GraphNode:
            parent = obj.obj
            if type(parent) == asm_ast.Block:
                # common case -- for CFG
                if parent.is_special():
                    label = parent.name
                else:
                    # put code in label
                    if this.use_block_content:
                        label = this.unparse(parent)
                    else:
                        label = parent.name
            else:
                # this would be for non-Block nodes, such as call
                label = parent.name
            
        elif type(obj) == graph.Edge:
            pass
        else:
            pass

        return label
    
    
