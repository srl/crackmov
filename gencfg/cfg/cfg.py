#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2467 $
#   $Author: arun $
#   $Date: 2015-06-21 18:16:51 -0500 (Sun, 21 Jun 2015) $


import sys
import os
import logging
logger = logging.getLogger('cfg')

import asm_ast
graphlibpath = os.path.join(os.path.dirname( __file__), "graph")
if not graphlibpath in sys.path:
    sys.path.append(graphlibpath)

sys.path.append("graph")
import graph

class CfgCollection (object):
    def __init__ (this, ast):
    	this.cfg_cache = None
    	this.ast = ast
    def get_cfgs(this):
    	if not this.cfg_cache:
    		logger.info("Constructing CFG")
    		this.cfg_cache = [a_cfg for a_cfg in create_all_CFGs(this.ast)]
    	return this.cfg_cache


def create_all_CFGs (ast):
    # enumerate all procedures and create their CFG
    g = ast.descendants_of_type(asm_ast.Procedure)
    for proc in g:
    	if not proc.dummy_external:
        	cfg = create_CFG(proc)
        	yield cfg


def create_CFG (proc):
    assert type(proc) == asm_ast.Procedure
    # create a graph
    cfg = graph.DirectedGraph(proc, 'CFG')

    # for each block, create a node, and its edges
    children = proc.descendants_of_type(asm_ast.Block)
    children = sorted(children, key=lambda x: x.start_address())
    for block in children:
        this_node = cfg.add_node(block)
        # find successors of each block
        for succ in block.successors():
            succ_node = cfg.add_node(succ)
            # and create edges
            this_node.add_edge_to(succ_node)
    return cfg


#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
