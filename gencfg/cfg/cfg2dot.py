#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1810 $
#   $Author: arun $
#   $Date: 2012-08-25 18:39:34 -0500 (Sat, 25 Aug 2012) $

import re
import graph
import dot_visitor
import asm_ast

class Annotator(dot_visitor.Annotator):
    def __init__ (this, unparser):
        super(type(this), this).__init__()
        this.unparser = unparser
        # zap indentations in the unparser
        if type(this.unparser) == dict:
        	for key in this.unparser:
        		unparser_x = this.unparser[key]
        		unparser_x.base_indent = ""
        elif this.unparser:
        	this.unparser.base_indent = ""

    def id(this, obj):
        result = "Unknown type"
        if type(obj) == graph.GraphNode:
            result = obj.id()
        elif type(obj) == graph.Edge:
            result = "Edges have no id"
        elif type(obj) == graph.DirectedGraph:
            result = obj.name
        return "\"%s\"" % result

    def unparse (this, obj):
    	# this method is defunct. It is retained, just in case if its
    	# called from somewhere. Look at the method annot(), below, for details.
        if this.unparser and (not type(this.unparser) == dict):
            return this.unparser.unparse(obj)
        # else use default
        return "%s" % obj
    
    def annot(this, obj):
        props = {}
        if type(obj) == graph.GraphNode:
            parent = obj.obj
            if type(parent) == asm_ast.Block:
                # common case -- for CFG
                if parent.is_special():
                    props['shape'] = 'oval'
                    props['label'] = parent.name
                else:
                    props['shape'] = 'box'
                    # put code in label
                    if type(this.unparser) == dict:
                    	# multiple unparsers are provided as dictionary
                    	# if there are multiple unparsers, generate
                    	# results from each unparser, and provide each as 
                    	# an attribute in dot
                    	# Its the callers responsibility to ensure that
                    	# the dictionary keys correspond to valid dot attributes
                    	for label in this.unparser:
                    		props[label] = this.unparser[label].unparse(parent)
                    elif this.unparser:
                    	# if there is only one parser, use its value as a label
                    	# the issue is simple enough
                    	props['label'] = this.unparser.unparse(parent)
                    else:
                    	# if there is no unparser, generate its string version
                    	props['label'] = "%s" % parent
            else:
                # this would be for non-Block nodes, such as call
                props['label'] = parent.name
                props['shape'] = 'oval'
            
        elif type(obj) == graph.Edge:
            pass
        else:
            pass

        str = ""
        sep = ""
        for key in props:
            value = props[key]
            value = re.sub(r"\n", "\\\\n", value)
            str += sep + "%s=\"%s\"" % (key, value)
            sep = " "
        if str == "":
            return None
        return "[" + str + "]"
    
    
