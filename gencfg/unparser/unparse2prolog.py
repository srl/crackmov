#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2486 $
#   $Author: arun $
#   $Date: 2015-07-06 17:55:04 -0500 (Mon, 06 Jul 2015) $

import asm_ast
import unparse_asm
import re

class Unparser (unparse_asm.Unparser):
    def __init__(this, show_lineno = True, show_address = True, show_code = True):
        super(type(this), this).__init__()
        this.show_lineno = show_lineno
        this.show_address = show_address
        this.show_code = show_code

    def default_sep (this):
        return ",\n"
 
    def program (this, obj, child_result):
        assert type(obj) == asm_ast.Program
        
        body = this.indent(obj) + "program('%s', [\n" % obj.name
        sep = ",\n"
        body += sep.join(child_result)
        body += "\n]).\n"
        return body


    def section (this, obj, child_result):
        assert type(obj) == asm_ast.Section
        sep = ",\n"
        line_indent = this.indent(obj)
        body = line_indent + "section('%s', [\n" % (obj.name)
        body += sep.join(child_result)
        body += "\n" + line_indent + "])"
        return body

    def procedure (this, obj, child_result):
        assert type(obj) == asm_ast.Procedure
        procname = 'unknown'
        if obj.name:
            procname = obj.name
            
        line_indent = this.indent(obj)
        sep = ",\n"
        body = line_indent + "procedure('%s', [\n" % procname
        body += sep.join(child_result)
        body += "\n" + line_indent + "])"
        return body
        
    def block (this, obj, child_result):
        assert type(obj) == asm_ast.Block
        blockname = obj.name

        line_indent = this.indent(obj)
        sep = ",\n"
        body = line_indent + "block('%s',[\n" % blockname
        body += sep.join(child_result)
        body += "\n" + line_indent + "])"
        return body
    
    def instruction(this, obj, child_result):
        assert type(obj) == asm_ast.Instruction
        qual = ""
        if obj.qual:
            qual = obj.qual
        code = "["
        sep = ""
        for c in obj.code:
            if c == '':
                break
            code += sep
            code += "0x"+c
            sep = ","
        code += "]"

        sep = ", "
        args = sep.join(child_result)

        # clean up mnemonic for Prolog syntax
        clean_mnemonic = obj.mnemonic
        result = re.match("\((<?P><mne>.+)\)", clean_mnemonic)
        if obj.mnemonic == "(bad)":
        	#clean_mnemonic = result.group('mne')
        	clean_mnemonic = "bad"
        
        if args:
            instr = "%s(%s)" % (clean_mnemonic, args)
        else:
            # no arguments
            instr = clean_mnemonic
        if (obj.qual):
            instr = "%s(%s)" % (obj.qual, instr)
        body = this.indent(obj) + "i("
        sep = ""
        argsep = ", "
        if this.show_lineno:
            body += sep + str(obj.lineno)
            sep = argsep
        if this.show_address:
            body += sep + obj.address
            sep = argsep
        if this.show_code:
            body += sep + code
            sep = argsep
        body += sep + instr
        body +=")"
        
        return body

    def register_operand (this, obj, child_result = []):
        assert type(obj) == asm_ast.RegisterOperand
        #return "register(%s)" % obj.reg
        return obj.reg
    def immediate_operand (this, obj, child_result = []):
        assert type(obj) == asm_ast.ImmediateOperand
        if obj.label:
            #result = "immediate(%s,'%s')" % (obj.value, obj.label)
            result = "'%s'" % obj.label
        else:
            #result = "immediate(%s)" % obj.value
            result = obj.value
        return result
    
    def immediate_offset (this, obj, child_result = []):
        assert type(obj) == asm_ast.ImmediateOffset
        return "immediate_offset(%s, %s)" % (obj.offset, obj.disp)

    def indirect_operand (this, obj, child_result = []):
        assert type(obj) == asm_ast.IndirectOperand
        # output format
        #     segreg:ptr(reg1 + reg2*xplier +/- disp)
        #   Only the values present are output..
        body = ""
        if obj.reg2:
            body += obj.reg2
            if obj.xplier:
                body += "*" + obj.xplier
        if obj.reg1:
            if not body == "":
                body = obj.reg1 + "+" + body
            else:
                body = obj.reg1
        if obj.disp:
            # obj.disp already has "+/-" prefix
            body += obj.disp

        if obj.segreg and (not body == ""):
            body += "+" + obj.segreg
        elif obj.segreg:
            body += obj.segreg

        ptr = obj.ptr
        if obj.ptr:
            ptr = obj.ptr[0] + "ptr"
        else:
            parent = obj.parent_of_type(asm_ast.Instruction)
            ptr = str(parent.operand_size())[0]+"ptr"

        return ("%s(%s)" % (ptr, body)).lower()
        
