#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1810 $
#   $Author: arun $
#   $Date: 2012-08-25 18:39:34 -0500 (Sat, 25 Aug 2012) $

import asm_ast
import unparse_asm

class Unparser (unparse_asm.Unparser):
    def __init__(this):
        super(type(this), this).__init__()

    def default_sep (this):
        return "\n"
    
    def program (this, obj, child_result):
        assert type(obj) == asm_ast.Program
        body = "program\n"
        body += this.default_sep().join(child_result)
        return body

    def section (this, obj, child_result):
        assert type(obj) == asm_ast.Section
        body = this.indent(obj) + "section\n"
        body += this.default_sep().join(child_result)
        return body

    def procedure (this, obj, child_result):
        assert type(obj) == asm_ast.Procedure
        body = this.indent(obj) + "procedure\n"
        body += this.default_sep().join(child_result)
        return body
        
    def block (this, obj, child_result):
        assert type(obj) == asm_ast.Block
        blockname = obj.name
        body = this.indent(obj) + "block\n"
        body += this.default_sep().join(child_result)
        return body
    
    def instruction(this, obj, child_result):
        assert type(obj) == asm_ast.Instruction
        qual = ""
        if obj.qual:
            qual = obj.qual

        instr = obj.mnemonic
        if (obj.qual):
            instr = "%s(%s)" % (obj.qual, instr)
        body = this.indent(obj)
        body += instr
        return body

        
