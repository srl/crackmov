#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1731 $
#   $Author: arun $
#   $Date: 2012-08-08 15:49:55 -0500 (Wed, 08 Aug 2012) $


import md5
import unparse_asm

class Unparser (unparse_asm.Unparser):
    def __init__(this, unparser):
        super(type(this), this).__init__()
        this.next_unparser = unparser

    def unparse (this, obj):
        result = this.next_unparser.unparse(obj)
        return md5.new(result).hexdigest()
