#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 2466 $
#   $Author: arun $
#   $Date: 2015-06-21 13:04:53 -0500 (Sun, 21 Jun 2015) $


import asm_ast

class Unparser (object):
    def __init__(this):
        this.base_indent = " "

    def indent(this, obj):
    	"""Line indentation for various type of objects"""
    	return this.base_indent * (obj.indent_level * 2)
    
    def unparse(this, obj):
        # unparse the children
        child_result = []
        if obj.children == None:
            print "Missing children: %s" % (type(obj))
        else:    
            for child in obj.sorted_children():
                if type(obj) == asm_ast.Procedure:
                    print child.start_address()
                if type(child) == asm_ast.Block:
                    print "block ", child.start_address()
                result = this.unparse(child)
                if result:
                    child_result.append(result)
                    
        # now unparse the parent    
        if type(obj) == asm_ast.Program:
            retval = this.program(obj, child_result)
        elif type(obj) == asm_ast.Section:
            retval = this.section(obj, child_result)
        elif type(obj) == asm_ast.Procedure:
            retval = this.procedure(obj, child_result)
        elif type(obj) == asm_ast.Block:
            retval = this.block(obj, child_result)
        elif type(obj) == asm_ast.Instruction:
            retval = this.instruction(obj, child_result)
        elif type(obj) == asm_ast.RegisterOperand:
            retval = this.register_operand(obj, child_result)
        elif type(obj) == asm_ast.ImmediateOperand:
            retval = this.immediate_operand(obj, child_result)
        elif type(obj) == asm_ast.ImmediateOffset:
            retval = this.immediate_offset(obj, child_result)
        elif type(obj) == asm_ast.IndirectOperand:
            retval = this.indirect_operand(obj, child_result)
        else:
            assert False
        # now deal with case where child produced a parse
        # and parent didn't
        if retval:
            # parent produced parse, go on
            pass
        elif len(child_result) == 0:
            # child didn't produce a parse, go on
            pass
        elif len(child_result) == 1:
            # child parse has exactly one value,
            # extract it, and return that
            retval = child_result[0]
        else:
            # child parse has multiple values
            # join them using a default joiner
            retval = this.default_sep().join(child_result)
        return retval

    # the methods to override to create a new unparser..
    
    def default_sep(this):
        return "\n"
    
    def program (this, obj, child_result):
        return None
    
    def section (this, obj, child_result):
        return None
    
    def procedure (this, obj, child_result):
        return None
    
    def block (this, obj, child_result):
        return "default block"
    
    def instruction (this, obj, child_result):
        return None
    
    def register_operand (this, obj, child_result = []):
        return None
    
    def immediate_operand (this, obj, child_result = []):
        return None
    
    def immediate_offset (this, obj, child_result = []):
        return None

    def indirect_operand (this, obj, child_result = []):
        return None


