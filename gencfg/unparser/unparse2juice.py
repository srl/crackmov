#  Author: Arun Lakhotia, University of Louisiana at Lafayette
#   $Revision: 1731 $
#   $Author: arun $
#   $Date: 2012-08-08 15:49:55 -0500 (Wed, 08 Aug 2012) $

import asm_ast
import unparse_asm
import logging
logger = logging.getLogger('unparser2juice')

# list of valid juice properties, as returned by Prolog
JUICE_PROPERTIES = ['code', 'semantics', 'simplifications', 'types',
                    'gen_code', 'gen_semantics', 'gen_simplifications', 'gen_types']

class Unparser (unparse_asm.Unparser):
    def __init__(this, selected_properties):
        super(type(this), this).__init__()

        this.selected_juice_properties = []
        if type(selected_properties) == str:
        	this.selected_juice_properties = [selected_properties]
        else:
        	this.selected_juice_properties = selected_properties
        # Make sure the properties selected are juice properties
        for property in this.selected_juice_properties:
            if not property in JUICE_PROPERTIES:
                logging.warn("Property '%s' is not a valid juice property. Ignored." % property)
    

    def default_sep (this):
        return "\n"
    def program (this, obj, child_result):
        assert type(obj) == asm_ast.Program
        
        body = this.indent(obj) + "program\n"
        body += this.default_sep().join(child_result)
        return body

    def section (this, obj, child_result):
        assert type(obj) == asm_ast.Section
        body = this.indent(obj) + "section\n"
        body += this.default_sep().join(child_result)

        return body

    def procedure (this, obj, child_result):
        assert type(obj) == asm_ast.Procedure

        body = this.indent(obj) + "procedure\n"
        body += this.default_sep().join(child_result)

        return body
        
    def block (this, obj, child_result):
        assert type(obj) == asm_ast.Block
        blockname = obj.name

        line_indent = this.indent(obj)
        body = line_indent
        if len(this.selected_juice_properties) == 1:
        	# if only one juice property is asked, do not
        	# output the property label
        	property = this.selected_juice_properties[0]
        	if property in obj.properties:
        		body += "%r"%obj.properties[property]
        else:
        	# otherwise output in <property>::<value> format
        	sep = ""
        	for property in this.selected_juice_properties:
        		if property in obj.properties:
        			## Caution -- not using default_sep()... should change
        			## to using it, to make the code more uniform
        			body += sep + "%s::%r\n" % (property, obj.properties[property])
        			sep = line_indent
        return body
    
   

        
