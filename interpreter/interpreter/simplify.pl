%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% Associativity
%  X + (Y + Z) => (X + Y) + Z
assoc_rule((+), (+), (+), (+)).
%  X + (Y - Z) => (X + Y) - Z
assoc_rule((+), (-), (+), (-)).
%  X - (Y - Z) => (X - Y) + Z
assoc_rule((-), (-), (-), (+)).
%  X - (Y + Z) => (X - Y) - Z
assoc_rule((-), (+), (-), (-)).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% Distributivity
% X * (Y + Z) => (X * Y) + (X * Z)
dist_rule((*), (+)).
dist_rule((/\), (\/)).
dist_rule(and, or).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% Commutativity
commutes((+)).
commutes((*)).
commutes(and).
commutes(or).
commutes(xor).
commutes(cmp).
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% Mapping from syntax/semantic domain operators to Prolog operators
prolog_primitive(X+Y, X+Y).
prolog_primitive(X-Y, X-Y).
prolog_primitive(X*Y, X*Y).
prolog_primitive(and(X,Y), X  /\ Y).
prolog_primitive(or(X,Y),  X \/ Y).
prolog_primitive(xor(X,Y), xor(X,Y)).
prolog_primitive(shl(X,Y), X << Y).
prolog_primitive(shr(X,Y), X >> Y).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% Evalte expression using Prolog
compute_exp(X, Y):-
   prolog_primitive(X, NewX), !,
   bump_simplification,
   Y is NewX.
compute_exp(cmp(A,A),1) :-!.
compute_exp(cmp(_,_),0).
compute_exp(X, X).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% simplify(+LVal1, -LVal2)
%   Simplifies an LVal. In an abstract domain this may be an expression.
%   Performs algebraic simplification using associativity and commutativity.
simplify(ThreeTerms, E) :-
   % ThreeTerms = N1 Op1 (N2 Op2 TermRest) 
   nonvar(ThreeTerms),
   ThreeTerms =.. [Op1, N1, TwoTerms],
   nonvar(TwoTerms),
   TwoTerms   =.. [Op2, N2, TermRest],
   number(N1),
   number(N2),
   assoc_rule(Op1, Op2, NewOp1, NewOp2), !,
   	AssExp =.. [NewOp1, N1, N2],
   	compute_exp(AssExp, Val),
   	% apply associativity rule
   	RestExp =.. [NewOp2, Val, TermRest],
   	simplify(RestExp, E).
   	
 simplify(ThreeTerms, E) :-
   % ThreeTerms = N1 Op1 (N2 Op2 TermRest) 
   nonvar(ThreeTerms),
   ThreeTerms =.. [Op1, N1, TwoTerms],
   nonvar(TwoTerms),
   TwoTerms   =.. [Op2, N2, TermRest],
   number(N1),
   number(N2),  	
   dist_rule(Op1, Op2), !,
   	   % N1 Op1 (N2 Op2 TermRest)
   	   %   => (N1 Op1 N2) Op2 (N1 Op2 TermRest)
   	   NewTerm1 =.. [Op1, N1, N2],
   	   NewTerm2 =.. [Op1, N1, TermRest],
   	   simplify(NewTerm1, Simple1),
   	   simplify(NewTerm2, Simple2),
   	   DistTerm =.. [Op2, Simple1, Simple2],
   	   simplify(DistTerm, E).
 
simplify(TwoTerms, Val) :-
   nonvar(TwoTerms),
   TwoTerms =.. [_Op1, N1, N2],
   % TwoTerms = N1 Op1 N2
   number(N1),
   number(N2),!,
   compute_exp(TwoTerms, Val).

simplify(TwoTerms, Val) :-
   % TwoTerms = N1 Op1 N2
   nonvar(TwoTerms),
   TwoTerms =.. [Op1, N1, N2],
   number(N2),
   commutes(Op1), !,
   NewTerms =.. [Op1, N2, N1], 
   simplify(NewTerms, Val).
 
simplify(X1-X2, E):-
   number(X2), !,
   X2a is -X2,
   simplify(X2a + X1, E).

simplify(E1, E2) :-
	simplify_algebraic(E1,E2),!,
	bump_simplification.
	
simplify(E, E).

simplify_algebraic(0+X2, E):-!,simplify(X2, E).
simplify_algebraic(1*X2, E):-!, simplify(X2, E).
simplify_algebraic(0*_X2, 0):-!.
simplify_algebraic(and(0,_X2), 0):-!.
simplify_algebraic(xor(X1,X1), 0):-!.
simplify_algebraic(sub(X1,X1), 0):-!.
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
