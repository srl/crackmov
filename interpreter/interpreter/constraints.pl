%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
state_to_constraints(State, Constraints) :-
	make_empty_constraints(EmptyConstraint),
	state_to_constraint_x(State, EmptyConstraint, Constraints).
	
	state_to_constraint_x([], ConstraintIn, ConstraintIn).
	state_to_constraint_x([S|State], ConstraintIn, ConstraintOut) :-
		(is_type_binding(S) 
		-> add_type_constraint(ConstraintIn, S, Constraint2)
		; is_value_binding(S) 
		  ->  make_value_binding(V1, V2, S), 
		      add_value_constraint(ConstraintIn, V1=V2, Constraint2)
		  ; % else its a state semantics
		     add_state_constraint(ConstraintIn, S, Constraint2)),
		state_to_constraint_x(State, Constraint2, ConstraintOut).
		

state_to_semantics(State, Semantics) :-
	state_to_semantics(State, [], Semantics).
	
	state_to_semantics([], S, S).
	state_to_semantics([S|RestState], Sin, Sout) :-
		((is_value_binding(S); is_type_binding(S))
		  -> % value or type binding, skip it
		    Sin2 = Sin
		  ; % else keep it
		  Sin2 = [S|Sin]),
		state_to_semantics(RestState, Sin2, Sout).

state_to_simplifications(State, Simplifications) :-
	state_to_simplifications(State, [], Simplifications).
	
	state_to_simplifications([], S, S).
	state_to_simplifications([S|RestState], Sin, Sout) :-
		(is_value_binding(S)
		  -> % value binding, keep it
		  make_value_binding(V1, V2, S),
		  Sin2 = [V1=V2|Sin]
		  ; % skip everything else
		  Sin2 = Sin),
		state_to_simplifications(RestState, Sin2, Sout).
	

		
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

make_empty_constraints(constraint([], [], [])).

add_value_constraint(constraint(Types, ValuesIn, State), Value, constraint(Types, [Value|ValuesIn], State)).
add_type_constraint(constraint(TypesIn, Values, State), Type, constraint([Type|TypesIn], Values, State)).
add_state_constraint(constraint(Types, Values, StateIn), State, constraint(Types, Values, [State|StateIn])).

replace_value_constraints(constraint(Types, _ValuesIn, State), NewValues, constraint(Types, NewValues, State)).
replace_type_constraints(constraint(_Types, Values, State), NewTypes, constraint(NewTypes, Values, State)).
replace_state_constraints(constraint(Types, Values, _State), NewStates, constraint(Types, Values, NewStates)).

get_type_constraints(constraint(Types, _Values, _State), Types).
get_value_constraints(constraint(_Types, Values, _State), Values).
get_state_constraints(constraint(_Types, _Values, State), State).

merge_constraints(constraint(T1,V1,S1), constraint(T2,V2,S2), constraint(T,V,S)) :-
	append(T1, T2, T),
	append(V1, V2, V),
	append(S1, S2, S).
	
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
