%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
watch_mem(_,_) :- get_flag(watch_mem, 0), !, fail.
watch_mem(MemAddr, Taint) :-
   number(MemAddr),
   memtrack(taint, Base, Size, Prefix),
   MemAddr >= Base,
   MemAddr =< Base+Size,
   Offset is MemAddr - Base + 1,!,
   make_taint(Prefix, Offset, Taint).

% watch_mem -- Fail if there is no taint

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

watch_taint(_) :- get_flag(watch_taint, 0), !.
watch_taint(X) :- var(X), !.
watch_taint(T) :- 
   has_taint(T, c), has_taint(T, g),!,
   get_program_state(ip, IP),
       get_program_state(loop, L),
       get_program_state(visited_loop, VL),
       (VL < L ->
          format('Found both taints - loop #~n at ~p - ~p~n', [L, IP, T])
       ; true),
       set_program_state(visited_loop, L).

watch_taint(T) :-
   get_program_state(loop, L),
   length(T, N), N > L, has_taint(T,g), !,
   get_program_state(ip, IP),
   get_program_state(visited_loop2, VL),
   (VL < L ->
      format('Found first union of g tags: Loop # ~n at ~p: ~p~n', [L, IP, T])
   ; true),
   set_program_state(visited_loop2, L).

watch_taint(_).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

taint_watched_mems([], _MemAddr, []).
taint_watched_mems([X|Xs], MemAddr, [TX|TXs]) :-
    (watch_mem(MemAddr, Taint) 
      ->  ensure_taint(TX, X, Taint)
      ; TX = X),
    MemAddr1 is MemAddr + 1,
    taint_watched_mems(Xs, MemAddr1, TXs).

ensure_taint(TX, X, Taint) :-
   is_tainted(X),!,
   taint_pair(X, X1, Taint1),
   merge_taint(Taint, Taint1, Taint2),
   taint_pair(TX, X1, Taint2).
ensure_taint(TX, X, Taint) :- taint_pair(TX, X, Taint).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
watch_lookup(_,_,_) :- get_flag(watch_lookup, 0), !.
watch_lookup(MemAddr, _NBytes, Number) :-
   number(MemAddr),
   memtrack(lookup, Base, Size, Label),
   MemAddr >= Base,
   MemAddr =< Base + Size -1,!,
   get_program_state(loop, L),
   get_program_state(ip, IP),
   get_program_state(visited_lookup, VL),
   (VL < L ->
     format('Loop #~n Update of ~p ~p at ~p by value ~p~n', [L, MemAddr, Label, IP, Number])
    ; true).

watch_lookup(_,_,_).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

watch_update(_,_,_) :- get_flag(watch_update, 0), !.
watch_update(MemAddr, _NBytes, Value) :-
   number(MemAddr),
   memtrack(update, Base, Size, Label),
   MemAddr >= Base,
   MemAddr =< Base + Size -1,!,
   get_program_state(loop, L),
   get_program_state(ip, IP),
   get_program_state(visited_update, VL),
   (VL < L ->
     format('Loop #~n Update of ~p ~p at ~p by value ~p~n', [L, MemAddr, Label, IP, Value])
    ; true).

watch_update(_,_,_).


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
watch_instruction(_) :- get_flag(watch_instruction, 0), !.
watch_instruction(MemAddr) :-
    memtrack(instruction, MemAddr, ProcName), !,
    %memtrack(_, SelectorAddress, _, callselect),
    SelectorAddress = 0x80e1cbc,
    lookup_pgm_mem(SelectorAddress, 4, TNumber),
    (taint_pair(TNumber, MemAddr, _Taint)
      -> format('Entering ~p - ~p~n',[MemAddr, ProcName])
      ; true),

    Switch1 = 0x80e1cac,
    lookup_pgm_mem(Switch1, 4, TSwitch),
    (taint_pair(TSwitch, 1, _)
      -> format('Falling through ~p - ~p~n', [MemAddr, ProcName])
    ; true).




watch_instruction(_).

