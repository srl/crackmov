%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  Semantic Domain: RVal
%     RVal = Number | Exp | RVal:(RVal, n1, n2) 


extract_small_value(LargeVal, LowBit, HighBit, SmallVal) :-
	number(LargeVal), !,
	make_mask(LowBit, HighBit, Mask),
	% Patch with inverted mask
    MaskedLargeVal is LargeVal /\ (\ Mask),
    SmallVal is MaskedLargeVal >> LowBit.

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% patch_larger_value(LargeVal, SmallVal, (LargeVal:SmallVal)).
% Representation of patched value: Base:(Value,LowBit,HighBit)
patch_larger_value((_R, LowBit,HighBit), LargeVal, SmallVal, PatchVal) :-
	patch_larger_value_x(LargeVal, SmallVal, LowBit, HighBit, PatchVal).

% previously patched, in exactly the same location.	
patch_larger_value_x((LargeVal:(_PrevSmallVal,LowBit,HighBit)), 
                                NewSmallVal, LowBit, HighBit, 
                     (LargeVal:(NewSmallVal,LowBit,HighBit))) :-!.

% previously patched, new patch overlaps towards highbit
patch_larger_value_x((LargeVal:(PrevSmallVal,PrevLowBit,HighBit)), 
                                NewSmallVal, NewLowBit, HighBit, 
                     (LargeVal:(PatchVal,FinalLowBit,HighBit))) :-!,
     % recurse on the larger value.
      NewLowBit < PrevLowBit
     -> % New value is bigger (has more bits), throw away previous patch
        PatchVal = NewSmallVal, FinalLowBit = NewLowBit
      ; % Previous value is bigger, need to preserve extra bits
        % apply recursive patch, but remember that te PrevSmallVal
        % itself may not be aligned at 0, so will need to shift the
        % patch relative to the previous Low Bit
        RecLowBit is NewLowBit - PrevLowBit,
        RecHighBit is HighBit - (RecLowBit - NewLowBit),
        patch_larger_value_x(PrevSmallVal, NewSmallVal, RecLowBit, RecHighBit, RecPatchVal),
        PatchVal = (PrevSmallVal:(RecPatchVal,PrevLowBit,HighBit)). 

% previously patched, new patch overlaps towards lowbit
patch_larger_value_x((LargeVal:(PrevSmallVal,LowBit,PrevHighBit)), 
                                NewSmallVal, LowBit, NewHighBit, 
                     (LargeVal:(PatchVal,LowBit,FinalHighBit))) :-!,
     % recurse on the larger value.
      NewHighBit > PrevHighBit
     -> % New value is bigger (has more bits), throw away previous patch
        PatchVal = NewSmallVal, FinalHighBit = NewHighBit
      ; % Previous value is bigger, need to preserve extra bits
        % apply recursive patch, but remember that te PrevSmallVal
        % itself may not be aligned at 0, so will need to shift the
        % patch relative to the previous Low Bit
        RecLowBit is 0,
        RecHighBit is NewHighBit - LowBit,
        patch_larger_value_x(PrevSmallVal, NewSmallVal, RecLowBit, RecHighBit, RecPatchVal),
        PatchVal = (PrevSmallVal:(RecPatchVal,LowBit,PrevHighBit)). 
                                          
patch_larger_value_x(LargeVal, SmallVal, LowBit, HighBit, PatchVal) :-
   % Both values are numbers
   number(LargeVal), number(SmallVal), !,
   patch_numbers(LargeVal, SmallVal, LowBit, HighBit, PatchVal).
   
patch_larger_value_x(LargeVal, SmallVal, LowBit, HighBit, 
         (LargeVal:(SmallVal,LowBit,HighBit))).
 
patch_numbers(LargeVal, SmallVal, LowBit, HighBit, PatchVal) :-
	make_mask(LowBit, HighBit, Mask),
%	format('Mask: ~16r~n', Mask),
	MaskedLargeVal is LargeVal /\ Mask,
%	format('Masked Large Val: ~16r~n', MaskedLargeVal),
	PatchSmallVal is SmallVal << LowBit,
%	format('Patch Small Val: ~16r~n', PatchSmallVal),
	PatchVal is MaskedLargeVal \/ PatchSmallVal,
%	format('Patchval: ~16r~n', PatchVal),
	true.
	
make_mask(LowBit, HighBit, Mask) :-
	BigNum = (1<<32) - 1,
	LeftMask is (BigNum << HighBit) /\ BigNum,
	RightMask is 2^LowBit-1,
	Mask is LeftMask \/ RightMask.
