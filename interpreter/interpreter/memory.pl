%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

% MODEL MEMORY
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%     MEMORY UPDATE AND LOOKUP
%       For reference: http://www.swi-prolog.org/howto/database.html
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

:- dynamic '$dynamic_mem'/2.
%     '$dynamic_mem'(Address, NumberList)
%             where Address is DoubleWord boundary

clean_dmem :- retractall('$dynamic_mem'(_,_)).

lookup_dmem(Base,ByteList) :-
    '$dynamic_mem'(Base, ByteList), !.

update_dmem(Base, NewList) :-
    retractall('$dynamic_mem'(Base,_)),
    asserta('$dynamic_mem'(Base, NewList)).


show_dmem1 :-
    '$dynamic_mem'(Base, ByteList),
    print(Base), print(':'), print(ByteList),nl,
    fail.


show_dmem :-
    findall(Base-ByteList,  '$dynamic_mem'(Base, ByteList), AllDmems),
    keysort(AllDmems, SortedDmems),
    show_dmem_x(SortedDmems).
show_dmem_x([]).
show_dmem_x([Base-ByteList|Xs]) :-
    print(Base), print(':'), print(ByteList),nl,
    show_dmem_x(Xs).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup_mem(+Base, -ByteList)
%    looks up memory for list of bytes at address Base
%    first checks for dynamic memory, and then for program memory.
lookup_mem(Base,ByteList) :-
    % check dynamic memory first
    lookup_dmem(Base, ByteList1), !,
    taint_watched_mems(ByteList1, Base, ByteList).

lookup_mem(Base, TaintList) :-
    % lookup original program memory second
    mem(Base, ByteList),
    % fill up the ByteList with 0s if it is smaller than 16
    (length(ByteList, N),
     N < 16
     -> N1 is 16 - N,
        init_mem(N1, RestMem),
        append(ByteList, RestMem, ByteList1)
      ; ByteList = ByteList1),
    taint_watched_mems(ByteList1, Base, TaintList).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

lookup_pgm_mem_bytes(_, 0, []) :- !.
lookup_pgm_mem_bytes(MemAddr, Nbytes, [Byte|NumberList]) :-
    Base is (MemAddr div 16) * 16,
    Offset is MemAddr mod 16,
    lookup_mem(Base, ByteList), !,
    nth(Offset, ByteList, Byte),
    MemAddr1 is MemAddr+1,!,
    Nbytes1 is Nbytes - 1,
    lookup_pgm_mem_bytes(MemAddr1, Nbytes1, NumberList).
%% lookup_pgm_mem_bytes(_MemAddr, Nbytes, NumberList) :-
%%      Nbytes > 0,
%%      init_mem(Nbytes, NumberList).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  lookup_string(+MemAddr, -List)
%     MemAddr is address of a null terminated C string
%     returns List of code upto, but not including the Null character
lookup_string(MemAddr, List) :-
    lookup_pgm_mem_bytes(MemAddr, 1, [ByteVal]),
    taint_pair(ByteVal, Byte,_Tag),
    (Byte = 0
     -> List = []
     ; MemAddr1 is MemAddr + 1,
       List = [Byte|List1],
       lookup_string(MemAddr1, List1)).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

% update_mem(+Sin, +MemAddr, -Val, -Sout)
%    update value of mem and return 
update_mem(Sin, memdw(MemAddr), RVal, Sin) :-
    update_mem(MemAddr, 4, RVal).
update_mem(Sin, memw(MemAddr), RVal, Sin) :-
    update_mem(MemAddr, 2, RVal).
update_mem(Sin, memb(MemAddr), RVal, Sin) :-
    update_mem(MemAddr, 1, RVal).

update_mem(TMemAddr, Nbytes, TaintValue) :-
    taint_pair(TaintValue, Number, VTaint),
    taint_pair(TMemAddr, MemAddr, MemTaint),
    merge_taint(MemTaint, VTaint, Taint),
    make_little_endian(Number, Nbytes, LittleEndianList),
    taint_to_list(LittleEndianList, Taint, TaintedList),
    update_mem_array(MemAddr, TaintedList),
    watch_update(MemAddr, Nbytes, TaintValue).

update_mem_array(_MemAddr, []) :-!.
update_mem_array(MemAddr, [X|Xs]) :-
    update_mem_byte(MemAddr, X),
    MemAddr1 is MemAddr + 1,
    update_mem_array(MemAddr1, Xs).

update_mem_byte(MemAddr, Byte) :-
    Base is (MemAddr div 16) * 16,
    Offset is MemAddr mod 16,
    (lookup_mem(Base, ByteList); 
      init_mem(16, ByteList)),!,
    update_mem_row(Offset, ByteList, [Byte], NewList),
    update_dmem(Base, NewList).


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% update_mem_row(+Offset, +OrigByteList, +UpdateList, -NewList).
%      NewList is created by bytes in OrigByteList starting at Offset 
%           by the UpdateList
update_mem_row(0, OrigByteList, UpdateList, NewList) :-!,
    update_list_prefix(UpdateList, OrigByteList, NewList).
update_mem_row(N1, [O|OrigByteList], UpdateList, [O|NewList]) :-
    N1 > 0, !,
    N is N1 - 1,
    update_mem_row(N, OrigByteList, UpdateList, NewList).

% update_list_prefix(Update, Original, New)
%      New is created by replacing Update in front of Original
%      Update cannot be longer than Original

update_list_prefix([],X, X).
update_list_prefix([Y|Ys], [_X|Xs], [Y|Zs]) :-
    update_list_prefix(Ys, Xs, Zs).

init_mem(0, []).
init_mem(N1, [0|Xs]) :- N1 > 0, N is N1 - 1, init_mem(N, Xs).


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup_mem(+Sin, +MemAddr, -Val, -Sout)
%    lookup value of mem and return 
lookup_mem(Sin, memdw(MemAddr), RVal, Sin) :-
    lookup_pgm_mem(MemAddr, 4, RVal).
lookup_mem(Sin, memw(MemAddr), RVal, Sin) :-
    lookup_pgm_mem(MemAddr, 2, RVal).
lookup_mem(Sin, memb(MemAddr), RVal, Sin) :-
    lookup_pgm_mem(MemAddr, 1, RVal).

% lookup_pgm_mem(+MemAddr, +Nbytes, -Number).
lookup_pgm_mem(TMemAddr, Nbytes, TNumber) :-
       taint_pair(TMemAddr, MemAddr,AddrTaint), 
       number(MemAddr),
       (lookup_pgm_mem_bytes(MemAddr, Nbytes, ByteList)
        ->
	taint_from_list(ByteList, NumberList, ValueTaint),
        little_endian(NumberList, Number),
	merge_taint(AddrTaint, ValueTaint, Taint),
	taint_pair(TNumber, Number, Taint),
        ((is_tainted(Taint); logging_on(3)) -> 
             print(['---', MemAddr, '[0:', Nbytes, ']', ByteList]),
	     print(['numbers only', NumberList, 'result', Number, TNumber, Taint]),nl; true)

        ; default_value('$mem'(MemAddr, Nbytes), TNumber)),

       watch_lookup(MemAddr, Nbytes, TNumber).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
make_little_endian(0, 0, []).
make_little_endian(Number, Nbytes, [Low|HiList]) :-
         Nbytes > 0,
         Low is Number mod 256,
         Hi is Number div 256,
         Nbytes1 is Nbytes - 1,
         make_little_endian(Hi, Nbytes1, HiList).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''

% mem_test(1, [mov(eax,dptr(0x80e1c9c+ds)), mov(dptr(0x80e1c90), eax), mov(ebx,dptr(eax))]).


