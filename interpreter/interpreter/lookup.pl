%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup_setmark(+Sin, -Sout)
%   Set a mark in the state to delineate affect of instructions
%   Used for operational semantics
lookup_setmark(I, Sin, [inst=I|Sin]) :- use_operational_semantics,!.
lookup_setmark(_I, Sin, Sin).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% undef_lval(+LVal, +Sin, -Sout).
%   delete any value assinged to LVal in state Sin
%   return updated state.

undef_lval(LVal, Sin, Sout) :- 
   use_operational_semantics, !, 
   (register(LVal) 
      -> Sin = Sout
      ;  default_value(LVal, DVal), Sout = [LVal=DVal|Sin]).
undef_lval(LVal, Sin, Sout) :- undef_lval_x(LVal, Sin, Sout).

undef_lval_x(_LVal, [], []).
undef_lval_x(LVal, [LVal=_RVal|RestState], RestState2) :-!,
   % recurse, just in case there are multiple values
   undef_lval_x(LVal, RestState, RestState2).
undef_lval_x(LVal, [G|RestState], [G|RestState2]) :-
   undef_lval_x(LVal, RestState, RestState2).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup(+Sin, +LVal, Value) 
%   return Value assigned to LVal in state Sin
lookup([LVal=RVal|_], LVal, RVal) :- !.
lookup([_G|RestState], LVal, RVal) :- lookup(RestState, LVal, RVal).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup_state(+Sin, +LVal, -RVal, -Sout)
%   lookup value RVal of LVal in state Sin.
%     if not found, create default value and 
%   return RVal and updated state Sout
 
lookup_state(Sin, LVal, RVal, Sin) :-
	% normal case - LVal explicitly defined in Sin
    lookup(Sin, LVal, RVal), !.

lookup_state(Sin, LVal, RVal, Sout) :-
    register(LVal),!,
    % if LVal is a register, see if its implicity defined
    lookup_register(Sin, LVal, RVal, Sout).

lookup_state(Sin, LVal, RVal, Sout) :-
    lookup_mem(Sin, LVal, RVal, Sout).

lookup_state(Sin, LVal, RVal, Sin) :-
	% No value for LVal, implicit or explicit
	% use default value
    throw(['Lookup_state failed', LVal, Sin]),
    default_value(LVal, RVal). 

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% update_state(+LVal, +RVal, +Sin, -Sout).
%   Sout is the result of updating in Sin the value of LVal.
%   LVal is now bound to RVal.
%   If LVal is a register, then the update also triggers update
%   of all the aliased registers.

update_state(LVal, RVal, Sin, Sout) :-
    register(LVal),!,
    update_registers(LVal, RVal, Sin, Sout).

update_state(LVal, RVal, Sin, Sout) :-
    update_mem(Sin, LVal, RVal, Sout), !.


update_state(LVal, RVal, Sin, _Sout) :- throw(['Update_state -- both registers and mem updated failed', LVal, RVal, Sin]).

update_state(LVal, RVal, Sin, Sout) :-
	% its not a register, keep the whole thing
	% TODO: at some point when we improve memory pointer logic
	% the changes will be made here.
    update_state_x(LVal, RVal, Sin, Sout).

update_state_x(LVal, RVal, Sin, [LVal=RVal|Sin]) :-
	% if operational semantics is being used
	% keep the history of state updates.
	% This cannot be used for filtering semantic preserving rules
	% since the history will interfere.
	use_operational_semantics, !.
	
update_state_x(LVal, RVal, Sin, Sout) :-
   % if operational semantics is NOT being used
   % remove previous definitions, before including new definitions.
   % MUST USE this for filtering semantic preserving rules.	
   undef_lval(LVal, Sin, Sout1),
   % if RVal is default value, no change is needed.
   (default_value(LVal, RVal) -> Sout = Sout1   ; Sout = [LVal = RVal|Sout1]).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lookup_register(+Sin, +LVal, -RVal, -Sout)
%     If LVal is a register, checks if the parent register
%     is defined in Sin, and if so it extracts value
%     from the parent register... if value cannot be extracted
%     it fails. If parent is not defined, it recurses to its parent
%     Sout records decision, if needed.
lookup_register(Sin, LVal, RVal, Sout) :-
   register(LVal, _, BigReg, (LowBit,HighBit)),
   (lookup(Sin, BigReg, BigVal)
    ->    
     taint_pair(BigVal, BigC, BigT),
        extract_small_value(BigC, LowBit, HighBit, RValC),
        taint_pair(RVal, RValC, BigT),
    	Sin = Sout  % no decision to record in State
    ; 
        lookup_register(Sin, BigReg, BigVal, Sout),
        taint_pair(BigVal, BigC, BigT),
        extract_small_value(BigC, LowBit, HighBit, RValC),
        taint_pair(RVal, RValC, BigT)
   ), 
   !.
lookup_register(Sin, LVal, _, _) :-
    throw(['Lookup register failed', LVal, Sin]).
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% update_registers(+LVal, +RVal, +Sin, -Sout).
%   LVal must be a register. It may update multiple registers.
%   Sout is the result of updating in Sin the value of LVal.
%   LVal is now bound to RVal.
%   If LVal is a register, then the update also triggers update
%   of all the aliased registers.

update_registers(LVal, RVal, Sin, Sout) :-
    register_aliases(LVal, BiggerRegs, SmallerRegs),!,
    % LVal is a register, update parts of bigger registers
    register(LVal, _, _, Locs),
    update_bigger_registers(BiggerRegs, Locs, RVal, Sin, Sout1),
    % clobber smaller registers completely
    clobber_smaller_registers(SmallerRegs, Sout1, Sout2),
    update_state_x(LVal, RVal, Sout2, Sout).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  update_bigger_registers(+BiggerRegisters, (+LowBit, +HighBit), +RVal, +Sin, -Sout)
%      BiggerRegisters: List of registers
%      LowBit, HighBit: The range of bits aliased in the big register
%      Reg: name of the register
%      (LowBit, HighBit) give the range of bits to be modified.
%          example (eax, 0, 8).

update_bigger_registers([], _Locs, _RVal, Sin, Sin).
update_bigger_registers([Reg|Regs], Locs, SmallVal, Sin, Sout3) :-
   Locs = (LowBit,HighBit),
   lookup_state(Sin, Reg, LargeVal, Sout1),
   taint_pair(SmallVal, SmallC, SmallT),
   taint_pair(LargeVal, LargeC, LargeT),
   patch_larger_value((Reg,LowBit,HighBit), LargeC, SmallC, PatchC),
   merge_taint(SmallT, LargeT, PatchT),
   taint_pair(PatchVal, PatchC, PatchT),
   update_state_x(Reg, PatchVal, Sout1, Sout2),
   update_bigger_registers(Regs, Locs, SmallVal, Sout2, Sout3).

% clobber_smaller_registers(+SmallerRegisters, +Sin, -Sout)
%   the smaller registers are completely zapper, so we need
%   only the name of teh register
clobber_smaller_registers([], Sin, Sin).
clobber_smaller_registers([Reg|Regs], Sin, Sout2) :-
   undef_lval(Reg, Sin, Sout1),
   clobber_smaller_registers(Regs, Sout1, Sout2).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  filter_constraints(+Sin, -Sout)
%    Sout is the same as Sin, except the type and value constraints are removed.
%
filter_constraints([],[]).
filter_constraints([C|Xs], Ys) :-
    is_binding(C), !,
	filter_constraints(Xs, Ys).
filter_constraints([X|Xs], [X|Ys]) :-
	filter_constraints(Xs, Ys).
	
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% record_value_constraint(+Const, +Sin, -Sout).
%   record the constraint in Sout, if constraint tracking is on
%   and simplification was performed.
record_value_constraint(Val, NewExpr, Sin, [Constraint|Sin]) :-
	constraint_on,
	make_value_binding(Val, NewExpr, Constraint), !. 
record_value_constraint(_V, _E, Sin, Sin).

% record_type_constraint(+Const, +Sin, -Sout).
%   record the constraint in Sout, if constraint tracking is on
%   and simplification was performed.
record_type_constraint(Var, Type, Sin, [Constraint|Sin]) :-
	constraint_on,
	make_type_binding(Var, Type, Constraint), !.
record_type_constraint(_V, _T, Sin, Sin).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% clean_value_constraints(Values, CleanValues)
%   Value constraints are recored in a State in the form value(X) = Y.
%   This is done for convenience, so the constraints can be distinguished
%   from other entries in the State.
%   When presenting to the user, though, the form X = Y is more appropriate.
%   Do such cleaning here.

clean_value_constraints([], []).
clean_value_constraints([VC| Values], [Var=Val|CleanValues]) :-
	make_value_binding(Var, Val, VC),
	clean_value_constraints(Values, CleanValues).
	
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
make_value_binding(Var, Value, (value(Var) = Value)).
make_type_binding(Var, Type, (type(Var) = Type)).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

is_type_binding(Key=Value) :- nonvar(Key), make_type_binding(_, _, Key=Value).
is_value_binding(Key=Value) :- nonvar(Key), make_value_binding(_, _, Key=Value).

is_binding(X) :- is_type_binding(X); is_value_binding(X).
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
