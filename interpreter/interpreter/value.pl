%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

default_value(LVal, def(LVal)) :- throw('Creating default value').


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
use_taint :- get_flag(use_taint, 1).
disable_taint :- set_flag(use_taint, 0).
enable_taint :- set_flag(use_taint, 1).

:- disable_taint.

taint_pair(ValC, ValC, X) :- \+ use_taint, !, (var(X) -> X=[];true).
taint_pair(_,_,_) :- \+use_taint, !, throw('taint problem').
% taint_pair(TaintPair, ConcreteVal, TaintSet)
taint_pair(Val, ValC, ValT) :- 
    nonvar(ValC), 
    nonvar(ValT), 
    ValT = [],!,
    Val = ValC.

taint_pair(Val, ValC, ValT) :- 
    var(Val),
    nonvar(ValC), 
    var(ValT), 
    ValT = [],!,
    Val = ValC.

taint_pair(taint(ValC, ValT), ValC, ValT).
taint_pair(ValC, ValC, []) :- nonvar(ValC), number(ValC).

taint_to_list([], _, []).
taint_to_list([X|Xs], Taint, [TX|Ts]) :- taint_pair(TX, X, Taint), taint_to_list(Xs, Taint, Ts).

taint_from_list([], [], []).
taint_from_list([TaintedVal|TaintVs], [ValC|Cs], ValTs) :-
    taint_pair(TaintedVal, ValC, ValT),
    taint_from_list(TaintVs, Cs, Ts),
    merge_taint(ValT, Ts, ValTs).

merge_taint(Tag1, Tag2, TagUnion) :- 
   oset_union(Tag1, Tag2, TagUnion),
   watch_taint(TagUnion).

taint_pair_list([], [], []).
taint_pair_list([TP|Ts], [CV|Cs], Taints) :-
    taint_pair(TP, CV, Taint),
    taint_pair_list(Ts, Cs, X),
    merge_taint(Taint, X, Taints).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

reset_taint :- reset_gensym.
make_taint(Prefix, Taint) :- gensym(Prefix, Sym), oset_addel([], Sym, Taint).
make_taint(Prefix, Number, Taint) :- atom_concat(Prefix, Number, X), oset_addel([], X, Taint).
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

is_tainted(X) :- nonvar(X), X = taint(_,_).

has_taint([], _Type) :- false.
has_taint([T|_Ts], Type) :-
    atom_concat(Type, _, T),!.
has_taint([_|Ts], Type) :-
    has_taint(Ts, Type).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
