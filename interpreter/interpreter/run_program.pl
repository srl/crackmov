
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
program_state_flag(Variable, Flag):-
    concat_atom([program_state, Variable], ':', Flag).

reset_exit :-         set_flag(program_exit, 0).
set_program_exit :-   set_flag(program_exit, 1).
has_program_exited :- get_flag(program_exit, 1).

reset_loop :-
   program_state_flag(loop, Flag),
   set_flag(Flag, 0),
   set_program_state(visited_loop, -1),
   set_program_state(visited_loop2, -1),
   set_program_state(visited_lookup, -1),
   set_program_state(visited_update, -1).
bump_loop :-
    program_state_flag(loop, Flag),
    bump_flag(Flag, _).
set_program_state(Variable, Value) :- 
   program_state_flag(Variable, Flag),
   set_flag(Flag, Value).
get_program_state(Variable, Value) :- 
   program_state_flag(Variable, Flag),
   get_flag(Flag, Value).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

run_program :-
    reset_taint,
    clean_dmem,
    pgm_entry(StartNode),
    init_state(Sin),
    run_program(StartNode, Sin, _).


run_program(CurrentNode, Sin, Sout) :-
    (logging_on(1) -> format('Executing ~p~n', [CurrentNode]); true),
    node(CurrentNode, block(_, Block)),
    set_program_state(nodename, CurrentNode),
    run_program_x(Block, Sin, Sout1),
    % check if exit is setup
    (has_program_exited
    -> true
    ;
     next_node(CurrentNode, NextNode),
     (pgm_entry(NextNode) 
        ->
           bump_loop,
           get_program_state(loop, L)
           , (break_on(loop) -> format('Looping: ~w press ENTER~n', [L]), read_line(_); true)
        ; true),
     run_program(NextNode, Sout1, Sout)
     ).

next_node(CurrentNode, NextNode) :-
    % for now just pick the first node, and none else
    % this works for movfuscator just fine, since
    % there is only one successor node for all but the je blocks.
    % And for je blocks, the block to the jump target is the second edge.
    % and will not show up
    edge(CurrentNode, NextNode),!.

% run_program_x(+Pgm, +Sin, -Sout).
%   interpret program Pgm in state Sin
%    and return updated state Sout
run_program_x([], S, S).
run_program_x([i(N,Address, Inst)|Is], Sin, Sout3) :-
   (logging_on(3) -> format('Interpreting ~p~n', [Inst]); true),
   set_program_state(instruction_number, N),
   set_program_state(ip, Address),
   watch_instruction(Address),
   (i(Inst, Sin, Sout1) -> true; format('ERROR: Interpretation of ~p failed~n', [Inst]), Sout1=Sin),
   %% clear taint for esi -- its an index
   (false -> i(clear_taint(esp), Sout1, Sout2) ; Sout1 = Sout2),

   run_program_x(Is, Sout2, Sout3).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
break_flag(X, Y) :- atom(X), atom_concat('break', X, Y).
break_on(X) :- break_flag(X,Y), get_flag(Y, 1).
disable_break(X):- break_flag(X,Y), set_flag(Y,0).
enable_break(X) :- break_flag(X,Y), set_flag(Y,1).

trace_flag(X, Y) :- atom(X), atom_concat('trace', X, Y).
trace_on(X) :- trace_flag(X,Y), get_flag(Y, 1).
disable_trace(X):- trace_flag(X,Y), set_flag(Y,0).
enable_trace(X) :- trace_flag(X,Y), set_flag(Y,1).

:- disable_break(loop).
:- disable_trace(gets).

:- set_flag(watch_taint, 0).
:- set_flag(watch_update, 0).
:- set_flag(watch_lookup, 0).
:- set_flag(watch_instruction, 0).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
