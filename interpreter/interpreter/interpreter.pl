%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%  interpret_pgm_factorized(+Code, -FactorizedSemantics)
%    interpret Code, and return FactorizedSemantics
%       a factorized semantics, like semantics, is 
%       a list of key=value pair. In factorized semantics
%       common structures are replaced by prolog variables.
%  FactorizedSemantics is sorted, using Prolog's term order.
interpret_pgm_factorized(Code, FactorizedSemantics) :-
	interpret_pgm(Code, Semantics),
	% Semantics could be exponential, if printed
	% factorize it, to collapse common subtrees
	factorize_semantics(Semantics, FactorizedSemantics).

interpret_pgm_main_factorized(Code, FactorizedSemantics, FactorizedSimplifications) :-
	interpret_pgm_main(Code, Semantics, Simplifications),
	% Semantics could be exponential, if printed
	% factorize it, to collapse common subtrees
	factorize_semantics(Semantics, FactorizedSemantics),
	factorize_semantics(Simplifications, FactorizedSimplifications).

% factorize_semantics(+Semantics, -SortedFactorizedSemantics)
factorize_semantics(Semantics, SortedFactorizedSemantics) :-
	term_factorized(Semantics, Skeleton, Substitution),
	% selectively undo some factorization
	undo_factorization(Substitution, RemainingSubstitution),
	append(Skeleton, RemainingSubstitution, FactorizedSemantics),
	sort(FactorizedSemantics, SortedFactorizedSemantics).
	
	% undo_factorization(+Substitution, -RemainingSubstitution)
	%    Subtitution is a list of [PrologVar = Term...]
	%    when Term is some type of simple terms, the substitution
	%    is undid, by unifying the PrologVar and Term.
	%    Other forms of elements in the list are left untouched.
	undo_factorization([], []).
	undo_factorization([X = Y| Xs], Ys) :-
		(var(X), var(Y)
		 ; nonvar(Y),
		    % Y is of the form memb/?, memw/?, memdw/?
			(Y =.. [F|_], member(F, [memb, memw, memdw])
			% Y is of the form ,/2
		 	; Y = (_,_)
		 	% Y is of the form value(Z1) 
		 	; Y = value(_)
		 	; Y = (_ = _)
		 	% Y is of the form def(atomic)
		 	; Y = def(A), atom(A)
		 )), !,
		X = Y,
	 	undo_factorization(Xs, Ys).
	undo_factorization([X|Xs], [X|Ys]) :-
		undo_factorization(Xs, Ys).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

interpret_pgm_main(Pgm, Semantics, Simplifications) :-
	interpret_pgm(Pgm, State),
	state_to_semantics(State, Semantics),
	state_to_simplifications(State, Simplifications).
	
% interpret_pgm(+Pgm, -State)
%    interprets program, and returns State
%      State is a list of key=value pair, where
%      key is typically a register name, or memory location
%      but key may also contain special terms for retaining 
%      intermediate information from the interpretation
interpret_pgm(Pgm, State) :- interpret_pgm(Pgm, [], State).
	
% interpret_pgm(+Pgm, +Sin, -Sout).
%   interpret program Pgm in state Sin
%    and return updated state Sout
interpret_pgm([], S, S).
interpret_pgm([I|Is], Sin, Sout) :-
	enable_constraint,
   (logging_on(3) -> print(['Interpreting ', I]), nl; true),
   (i(I, Sin, Sout1) -> true; format('ERROR: Interpretation of ~w failed~n', I), Sout1=Sin), 
   interpret_pgm(Is, Sout1, Sout).
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% i(+Instr, +Sin, -Sout).
%   interpret insruction Instr in state Sin
%    and return updated state

i(I, Sin, Sout2) :-
	% for operational semantics
	% mark location in state where new instruction starts
	lookup_setmark(I, Sin, Sout1),
	i_x(I, Sout1, Sout2).

% A quick catalogue of Intel instructions
% http://www.penguin.cz/~literakl/intel/intel.html

i_x(exit, Sin, Sout) :-!,
    system_exit(Sin, Sout).
i_x(disable_log, Sin, Sin) :-!,
    disable_log.
i_x(enable_log(N), Sin, Sin) :-!,
    enable_log(N).
i_x(print_lval(Dest, Mesg), Sin, Sout) :- !,
   lval(Dest, Sin, LVal, Sout),
   format('~w: Lval ~p = ~p~n', [Mesg, Dest, LVal]).
i_x(print_rval(Dest, Mesg), Sin, Sout) :- !,
   rval(Dest, Sin, LVal, Sout),
   format('~w: Rval ~p = ~p~n', [Mesg, Dest, LVal]).

i_x(clear_taint(Src), Sin, Sout3) :- !,
   %% src is both lval and rval
   rval(Src, Sin, TRVal, Sout1),
   taint_pair(TRVal, RVal, _RTaint),
   lval(Src, Sout1, LVal, Sout2),
   update_state(LVal, RVal, Sout2, Sout3).


i_x(wait, Sin, Sin) :-
    print('Waiting, press ENTER to continue'), nl,
    read_line(_).

i_x(mov(Dest, Src), Sin, Sout3) :- !,
   rval(Src, Sin, Val, Sout1),
   lval(Dest, Sout1, Dest1, Sout2),
   update_state(Dest1, Val, Sout2, Sout3).
   
i_x(movs(Dest, Src), Sin, Sout3) :- !,
   rval_mem(Src, Sin, Val, Sout1),
   lval_mem(Dest, Sout1, Dest1, Sout2),
   update_state(Dest1, Val, Sout2, Sout3).

i_x(stos(Dest, Src), Sin, Sout3) :- !,
   rval(Src, Sin, Val, Sout1),
   lval_mem(Dest, Sout1, Dest1, Sout2),
   update_state(Dest1, Val, Sout2, Sout3).

i_x(lods(Dest, Src), Sin, Sout3) :- !,
   rval_mem(Src, Sin, Val, Sout1),
   lval(Dest, Sout1, Dest1, Sout2),
   update_state(Dest1, Val, Sout2, Sout3).

i_x(add(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(Val1+Val2, Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(adc(Dest, Src), Sin, Sout6) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(Val1+Val2, Sout2, Val, Sout3),
   update_state(carry, Val, Sout3, Sout4),
   lval(Dest, Sout4, Dest1, Sout5),
   update_state(Dest1, Val, Sout5, Sout6).
   
i_x(sub(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(Val1-Val2, Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(mul(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(Val1*Val2, Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(and(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(and(Val1,Val2), Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(or(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(or(Val1,Val2), Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(xor(Dest, Src), Sin, Sout5) :- !,
   rval(Dest, Sin, Val1, Sout1),
   rval(Src, Sout1, Val2, Sout2),
   e(xor(Val1,Val2), Sout2, Val, Sout3),
   lval(Dest, Sout3, Dest1, Sout4),
   update_state(Dest1, Val, Sout4, Sout5).

i_x(shl(Dest, Src), Sin, Sout5) :-!,
  rval(Dest, Sin, Val1, Sout1),
  rval(Src, Sout1, Val2, Sout2),
  e(shl(Val1, Val2), Sout2, Val, Sout3),
  lval(Dest, Sout3, Dest1, Sout4),
  update_state(Dest1, Val, Sout4, Sout5).

i_x(shr(Dest, Src), Sin, Sout5) :-!,
  rval(Dest, Sin, Val1, Sout1),
  rval(Src, Sout1, Val2, Sout2),
  e(shr(Val1, Val2), Sout2, Val, Sout3),
  lval(Dest, Sout3, Dest1, Sout4),
  update_state(Dest1, Val, Sout4, Sout5).

i_x(push(Src), Sin, Sout2) :- !,
    % push X = sub esp, 4; mov dptr(esp),X
    disable_constraint,
    i_x(sub(esp,4), Sin, Sout1),
    enable_constraint,
    i_x(mov(dptr(esp), Src), Sout1, Sout2).

i_x(pop(Dest), Sin, Sout3) :- !,
    % pop X = mov X, dptr(esp); undef dptr(esp); add esp, 4
    i_x(mov(Dest, dptr(esp)), Sin, Sout1),
    i_x(undef(dptr(esp)), Sout1, Sout2),
    disable_constraint,
    i_x(add(esp,4), Sout2, Sout3),
    enable_constraint.

i_x(undef(Aexp), Sin, Sout) :- !,
   % psuedo instruction added for undefining stack content
   lval(Aexp, Sin, LVal, _Sout),
   undef_lval(LVal, Sin, Sout).

i_x(cmp(Arg1,Arg2), Sin, Sout4) :-!,
  rval(Arg1, Sin, Val1, Sout1),
  rval(Arg2, Sout1, Val2, Sout2),
  e(cmp(Val1,Val2), Sout2, Val, Sout3),
  % print([cmp(Arg1,Arg2), ' value ', Val]),nl,
  % sets the flag register
  update_state(flag, Val, Sout3, Sout4).

i_x(lea(Arg1, Arg2), Sin, Sout) :-!,
  strip_ptr(Arg2, Arg2a),
  i_x(mov(Arg1, Arg2a), Sin, Sout).

i_x(je(SysCall), Sin, Sout) :- !,
    % print(je(SysCall)),nl,
    lookup(Sin, flag, TValue),
    taint_pair(TValue, Value, _),
    (Value = 1 ->
    system_call(SysCall, Sin, Sout)
    ; Sin = Sout).

i_x(_Unknown, S, S) :- !,
  % Instruction not implemented
  true.
  %format('% WARNING: Instruction ~w NOT implemented, treating as NOP~n', Unknown).

strip_ptr(dptr(X), X).
strip_ptr(wptr(X), X).
strip_ptr(bptr(X), X).
strip_ptr(none(X), X). % 

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% lval(+Aexp, +Sin, -LVal, -Sout)
%   compute the LVal of address expression Aexp
%     in state Sin. May need updating state
%     for undefined values. Returns updated state Sout.

% Syntax Domains:
%    Aexp := Register | dptr(Vexp) | bptr(Vexp)
%    Vexp := Register | Int | Vexp1 + Vexp2 | Vexp1 - Vexp2
% Semantic Domains
%     RVal ::= Int | def(LVal) RVal1 + RVal2 | RVal1 - RVal2
%     LVal ::= Reg | memdw(RVal) | memb(RVal) 

lval(Register, S, Register, S) :- register(Register), !.
lval(dptr(Vexp), Sin, memdw(RValExp), Sout) :-
    arg_e(Vexp, Sin, RValExp, Sout).   
lval(wptr(Vexp), Sin, memw(RValExp), Sout) :-
    arg_e(Vexp, Sin, RValExp, Sout).   
lval(bptr(Vexp), Sin, memb(RValExp), Sout) :-
    arg_e(Vexp, Sin, RValExp, Sout).   


% lval_mem(+Vexp, +Sin, -LVal, -Sout)
lval_mem(bptr(RegIndex, RegBase), Sin, memb(Val), Sout2) :-
	e(RegIndex + RegBase, Sin, Val, Sout1),
	inc_index(RegIndex, Sout1, Sout2). 
lval_mem(wptr(RegIndex, RegBase), Sin, memw(Val), Sout2) :-
	e(RegIndex + RegBase, Sin, Val, Sout1),
	inc_index(RegIndex, Sout1, Sout2). 
lval_mem(dptr(RegIndex, RegBase), Sin, memdw(Val), Sout2) :-
	e(RegIndex + RegBase, Sin, Val, Sout1),
	inc_index(RegIndex, Sout1, Sout2). 

% rval_mem(+Vexp, +Sin, -LVal, -Sout).
rval_mem(Vexp, Sin, LVal, Sout) :-
	lval_mem(Vexp, Sin, LVal, Sout).

	inc_index(RegIndex, Sin, Sout2) :-
		e(RegIndex + 1, Sin, Val, Sout1),
		update_state(RegIndex, Val, Sout1, Sout2).
		
%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% rval(+Vexp, +Sin, -RVal, -Sout)
%   compute value of expression Vexp in state Sin
%    return the value RVal and update state Sout 

rval(Register, Sin, Val, Sout) :-
   register(Register), !,
   lookup_state(Sin, Register, Val, Sout).

rval(dptr(Vexp), Sin, Val, Sout) :-!,
  arg_e(Vexp, Sin, RValExp, Sout1),
  lookup_state(Sout1, memdw(RValExp), Val, Sout).
  
rval(wptr(Vexp), Sin, Val, Sout) :-!,
  arg_e(Vexp, Sin, RValExp, Sout1),
  lookup_state(Sout1, memw(RValExp), Val, Sout).

rval(bptr(Vexp), Sin, Val, Sout) :-!,
  arg_e(Vexp, Sin, RValExp, Sout1),
  lookup_state(Sout1, memb(RValExp), Val, Sout).

rval(Exp, Sin, Val, Sout) :- 
  % argument to LEA can be a complex (enough) expression
  arg_e(Exp, Sin, Val, Sout).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% arg_e(+Vexp, +Sin, -RVal, -Sout)
%    evaluate expression in an argument, such as, eax+5 in dptr(eax+5).
%    for now mapping to e, which interprets operations in instructions
%    such as "add(ax,5)", etc.

arg_e(Vexp, Sin, RVal, Sout) :- e(Vexp, Sin, RVal, Sout).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  e(+Vexp, +Sin, -RVal, -Sout)
%    evaluate value of expression Vexp in state Sin
%      return resulting value RVal and update state Sout

e(Reg, Sin, Val, Sout) :-
   register(Reg), !,
   lookup_state(Sin, Reg, Val, Sout).
e(Val, Sin, Val, Sin) :- is_tainted(Val), !.
e(Val, Sin, Val, Sin) :- number(Val), !.

e(BinExpr, Sin, Val, Sout3) :-
  BinExpr =.. [Op, E1, E2],!,
  e(E1, Sin, E1Val, Sout1),
  e(E2, Sout1, E2Val, Sout2),
  taint_pair(E1Val, E1ValC, E1ValT),
  taint_pair(E2Val, E2ValC, E2ValT),
  NewExpr =.. [Op,E1ValC,E2ValC],
  reset_simplification,
  simplify(NewExpr, ValC),
  merge_taint(E1ValT, E2ValT, ValT),
  taint_pair(Val, ValC, ValT),
  (simplification_done 
    -> record_value_constraint(ValC, NewExpr, Sout2, Sout3)
    ; Sout3 = Sout2).

e(X, Sin, X, Sin).


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

interpret_rule(Rulenum) :-
	tr(Rulenum, Lhs, Rhs),
        format('Interpreting ~w -> ~w~n', [Lhs , Rhs ]),
        interpret_pgm(Lhs, [], Lsubs),
        interpret_pgm(Rhs, [], Rsubs),
        format('Interpretation LHS: ~w~n', [Lsubs]),
        format('Interpretation RHS: ~w~n', [Rsubs]),
        true . 

%''''''''''''''''''''''''''''''''''''''''''''''''''''''
init_state(S) :-
    update_state(ds, 0, [],S1),
    update_state(esi, 0x10ffffff, S1, S2),
    update_state(edi, 0x20ffffff, S2, S3),
    update_state(esp, 0x30ffffff, S3, S4),
    update_state(ebp, 0x30ffffff, S4, S),
    reset_exit,
    reset_loop.


%''''''''''''''''''''''''''''''''''''''''''''''''''''''

% system_call(+SysCall, +Sin, -Sout)
%    performs actions of the SysCall being called.
%    The stack has already been setup.
%    It doesn't remove parameters from the stack, though.
%    Since its not needed for movfuscator.
system_call('fgets@plt@entry', Sin, Sout):-
    % get parameter address from stack
    rval(dptr(esp+4), Sin, MemAddr, Sout),
    system_gets(MemAddr).

system_call('printf@plt@entry', Sin, Sout):- 
    % get parameter address from stack
    rval(dptr(esp+4), Sin, MemAddr, Sout),
    system_printf(MemAddr).
system_call('exit@plt@entry', Sin, Sout):- 
    system_exit(Sin, Sout).

system_exit(Sin, Sin) :-
    set_program_exit.

read_line(Input,Tail) :- read_line_to_codes(user_input, Input, Tail).
read_line(Input) :- read_line(Input, []).

system_gets(MemAddr) :-
    read_line(Input, Tail),
    Tail = [0],
    (trace_on(gets) -> format('Writing ~p to ~p~n', [Input, MemAddr]); true),
    taint_input(Input, TaintedInput),
    update_mem_array(MemAddr, TaintedInput).

taint_input([],[]).
taint_input([X|Xs],[TX|TXs]) :-
    make_taint('g', Taint),
    taint_pair(TX, X, Taint),
    taint_input(Xs, TXs).

system_printf(TMemAddr) :-
    taint_pair(TMemAddr, MemAddr, _),
    lookup_string(MemAddr, Codes),
    string_codes(Str,Codes),
    write(Str).


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
