
:- dynamic user:file_search_path/2.
:- multifile user:file_search_path/2.


get_flag(Flag, Value) :- flag(Flag, Value, 0), flag(Flag, _, Value).
bump_flag(Flag, Value) :- flag(Flag, Value, 0), Value1 is Value + 1, flag(Flag, _, Value1).
set_flag(Flag, Value) :- flag(Flag, _, Value).

:- prolog_load_context(directory, H), atom_concat(H, '/../', FilePath), set_flag(loadhome, FilePath).

user:file_search_path(code, CodePath) :- prolog_load_context(directory, CodePath).

:- [ 
     code('parameters/parameters.pl'),
     code('parameters/architecture.pl'),
     code('utility/pipe_line.pl'),
     code('utility/server.pl'),
     code('utility/support.pl'),
  
     code('interpreter/simplify.pl'),
     code('interpreter/value_patching.pl'),
     code('interpreter/lookup.pl'),
     code('interpreter/constraints.pl'),
     code('interpreter/memory.pl'),
     code('interpreter/value.pl'),
     code('interpreter/watch.pl'),
     code('interpreter/interpreter.pl'),
     code('interpreter/run_program.pl')
 ].


