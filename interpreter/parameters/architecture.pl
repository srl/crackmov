%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

% Register(Name, Size, Parent, Info).
%  Encoding of info about x86 registers.
%    when Parent is "none", Info gives the Type of register.
%    otherwise, Info gives the bit position in the parent register
%    aliased by this register.

%  Key for info:
%   gp = general purpose register
%   ix = index register
%   bp = base pointer
%   sp = stack pointer
%  flag, carry

register(eax, 32, none, gp).   
register(ebx, 32, none, gp).
register(ecx, 32, none, gp).
register(edx, 32, none, gp).

register(es,  32, none, ds).
register(ds,  32, none, ds).
register(esi, 32, none, ix).
register(edi, 32, none, ix).
register(ebp, 32, none, bp).
register(esp, 32, none, sp).

register(ax, 16, eax, (0,16)).
register(bx, 16, ebx, (0,16)).
register(cx, 16, ecx, (0,16)).
register(dx, 16, edx, (0,16)).

register(al, 8, ax, (0,8)).
register(bl, 8, bx, (0,8)).
register(cl, 8, cx, (0,8)).
register(dl, 8, dx, (0,8)).
register(ah, 8, ax, (8,16)).
register(bh, 8, bx, (8,16)).
register(ch, 8, cx, (8,16)).
register(dh, 8, dx, (8,16)).

register(flag, 1, none, flag).
register(carry, 1, none, carry).

register(Reg) :- register(Reg, _, _, _).

register_type(Reg, reg(Size, Type)) :- 	
    register(Reg, Size, none, Type).

register_type(Reg, reg(Size, Parent, Location)) :-
	register(Reg, Size, Parent, Location).

% register_aliases(Reg, Larger, Smaller)
register_aliases(Reg, Larger, Smaller) :- 
    register_larger(Reg, Larger), 
    register_smaller(Reg, Smaller).

register_larger(Reg, []) :-
	register(Reg, _, none, _),!.
register_larger(Reg, [Parent|Rest]) :-
	register(Reg, _, Parent, _),
	register_larger(Parent, Rest).


register_smaller(Reg, X) :-
	register_smaller_x([Reg], X).
	
	register_smaller_x([], []).
	register_smaller_x([Reg|Regs], AllDescendents) :-
		findall(Child, register(Child, _, Reg, _),  Children),
		append(Children, Regs, AllRegs),
		register_smaller_x(AllRegs, GrandChildren),
		append(Children, GrandChildren, AllDescendents).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

branch_instruction(Inst) :- 
	Inst =.. [F|_], 
	branch_mnemonics(BranchMnemonics),
	member(F, BranchMnemonics),!.

% List of mnemonics for instructions that directly modify the program counter
branch_mnemonics([ret, call, ja, jae, jb, jbe, jc, jcxz, je, jg, jge, 
                             jl, jle, jmp, jna, jnae, jnb, jnbe, jnc, 
                             jne, jng, jnge, jnl, jnle, jno, jnp, jns, 
                             jnz, jo, jp, jpe, jpo, js, jz]).

