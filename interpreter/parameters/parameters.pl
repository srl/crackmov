%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  use_operational_semantics
%    Flag to set, if operational semantics is to be used.
%    Use comment/uncomment to select the flag.
%:- dynamic use_operational_semantics/1.
% use_operational_semantics.
use_operational_semantics :- fail.

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% generalize_with_types
%    Also extract types for the variables, during generalization.
generalize_with_types.

factorize_terms.

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%
%  SIDE EFFECT -- Flag to remember if simplification was performed
reset_simplification:-
	set_flag(simplification_done,0).
bump_simplification :-
	bump_flag(simplification_done, _).

simplification_done:-
	get_flag(simplification_done,N), N > 0.

disable_constraint :-
	set_flag(record_constraint, 0).
enable_constraint :- true.
	%set_flag(record_constraint, 1).
constraint_on:-
    get_flag(record_constraint,1).

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

enable_log:- set_flag(binjuice_log, 1).
enable_log(N):- number(N), N > 0, set_flag(binjuice_log, N).
disable_log:- set_flag(binjuice_log, 0).
logging_on :- get_flag(binjuice_log, N), N > 0.
logging_on(N) :- get_flag(binjuice_log, M), N =< M.

:- disable_log.

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% output_to_stdout.
%   Set to true if you want output of process commands to go to stdout.
%   If it is false, output is written to a file, as defined by the 
%   file naming rules.
output_to_stdout :- fail.
continue_processing. % :-  bump_line_count(Count), Count < 100.

%
reset_line_count:- set_flag(line_counter, 0).
get_line_count(Count) :- get_flag(line_counter, Count).
bump_line_count(Count) :- bump_flag(line_counter, Count).
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
start_processing :- reset_line_count.
   	
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


make_rule(tr(_Num, Lhs, Rhs), [Lhs, Rhs]).
make_rule(rule(Lhs, Rhs, Constraints), [Lhs, Rhs, Constraints]).

merge_rule(rule(Lhs, Rhs, Constraints), rule(Lhs, Rhs, Constraints1), Constraints2) :-
	merge_constraints(Constraints1, Constraints2, Constraints).

rule_lhs(rule(Lhs, _Rhs, _C), Lhs).
rule_lhs(tr(_Num, Lhs, _Rhs), Lhs).

rule_rhs(rule(_Lhs, Rhs, _C), Rhs).
rule_rhs(tr(_Num, _Lhs, Rhs), Rhs).
rule_constraint(rule(_Lhs, _Rhs, C), C).


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

make_block(block(Code, EmptyConstraint), Code) :- 
    make_empty_constraints(EmptyConstraint).
make_block(block(Code, Constraint), Code, Constraint).

make_branch(branch(Code, EmptyConstraint), Code) :- 
    make_empty_constraints(EmptyConstraint).
make_branch(branch(Code, Constraint), Code, Constraint).

merge_program_constraints(block(Code, Constraint), MoreConstraint, block(Code, MergedConstraint)) :-
	merge_constraints(Constraint, MoreConstraint, MergedConstraint).
merge_program_constraints(branch(Code, Constraint), MoreConstraint, branch(Code, MergedConstraint)) :-
	merge_constraints(Constraint, MoreConstraint, MergedConstraint).

make_program(branch(Code, Constraint), branch, Code, Constraint).
make_program(block(Code, Constraint), block, Code, Constraint).

program_code(branch(Code, _Constraint), Code).
program_code(block(Code, _Constraint), Code).
program_constraints(block(_Code, Constraint), Constraint).
program_constraints(branch(_Code, Constraint), Constraint).
program_type(block(_,_), block).
program_type(branch(_,_), branch).


%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
