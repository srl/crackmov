%  Author: Arun Lakhotia, University of Louisiana at Lafayette
%   $Revision: $
%   $Author: arun $
%   $Date: $

%   GENERAL PURPOSE SUPPORT PREDICATES

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
% portray(N)  -- to overwrite printing of numbers
%      prints numbers greater than 16 in hex in 0xfffff format
%      
portray(N) :- portray_hex(N).

portray_hex(N) :- 
	number(N),
	N > 16,
	number_to_hex(N, Hex),
	print(Hex).

portray_hex(N) :- 
	number(N),
	N < -16,
	N1 is -N,
	number_to_hex(N1, Hex1),
	atom_concat('-', Hex1, Hex),
	print(Hex).

number_to_hex(N, Hex) :-
	number(N),
	format_to_chars('~16r', N, Chars), atom_chars(Atoms, Chars),
	atom_concat('0x', Atoms, Hex).

	
%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

nth(0, [X|_], X).
nth(N1, [_|Xs], R) :-
    number(N1),
    N1 > 0,
    N is N1 - 1,
    nth(N, Xs, R).

little_endian(NumberList, Number) :-
    little_endian(NumberList, 0, 1, Number).

% little_endian(NumberList, PartAnswer, BaseMultiple, Answer)
little_endian([Number], PartAnswer, BaseMultiple, Answer) :- 
    Number < 256,
    Answer is PartAnswer + BaseMultiple * Number.

little_endian([Number|Rest], PartAnswer, BaseMultiple, Answer) :- 
    Number < 256,
    PartAnswer2 is PartAnswer + BaseMultiple * Number,
    BaseMultiple2 is BaseMultiple * 256,
    little_endian(Rest, PartAnswer2, BaseMultiple2, Answer).


%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

atomize_list([], []).
atomize_list([X|Xs], [A|As]) :-
    term_to_atom(X,A),
    atomize_list(Xs, As).

format_list(L, Sep, F) :-
    atomize_list(L, A),
    atomic_list_concat(A, Sep, F).

format_list(L,F):-
    format_list(L, ';', F).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
