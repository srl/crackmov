%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%             PIPELINE PROCESSING
%  Use pipeline processing when the processing consists of a sequence of steps, chained by input/output
% 

% process_pipeline(+Datasource, +Basename, +Pipepred)
%   Basename is used to generate the name of the input and output files.
%   Datasource + Basename gives the name of the input file.
%   Datasource is defined using use:file_search_path(Datasource, DirectoryPath)
%     to get the directory where to find the input file.
%
%   Pipepred gives the name of a predicate that gives
%    the description of the pipe.
%
%   Example: process_pipeline(diff_rules, 'cmstp-4-5-mutated.diff.f', induction_pipe)
%            process_pipeline(asm_programs, 'vcode2ndGen_mutatedcmmon32.asm', summary_pipe)
process_pipeline(Datasource, Basename, Pipepred) :-
	start_processing,
	get_inputfile(Datasource, Basename, DataFilename),
   	format('Processing file ~w~n', [DataFilename]),	
	call(Pipepred, Pipe),
	process_pipeline_x(Pipe, DataFilename, Basename).
	
	process_pipeline_x([], _, _).
	process_pipeline_x([(Pred, Message, OutputType, Outputfn)| Ps], Input, Basename) :-
        get_outputfile(OutputType, Basename, Outfile),
        format('Processing ~w - writing to ~w~n', [Pred, Outfile]),
	     my_RedirectStream(Outfile),
	     format('%-------------------- ~w ------------------~n', Message),
	       ( catch(call(Pred, Input, Output), E1, (my_CloseStream(Outfile), write(E1),abort)),
	         catch(call(Outputfn, Output), E2, (my_CloseStream(Outfile), write(E2), abort)) 
	         -> Res=true; Res=false),
	      my_CloseStream(Outfile),
	      (Res = false -> format('CAUTION: Process ~w failed on ~w~n', [Message, Basename]); true),
	     process_pipeline_x(Ps, Output, Basename).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%  identity_transform(+Term1, -Term2)
%      Term1 = Term2
%      Its useful function, when we want to print the results using multiple
%       different print functions.

identify_transform(T, T).

%''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
%    SUPPORT FUNCTIONS
%

get_inputfile(Datasource, Basename, DataFilename) :-
	FilePattern =.. [Datasource, Basename],
    absolute_file_name(FilePattern, DataFilename, [access_mode(read)]).
    
get_outputfile(_OutputType, _Basename, 'user_output') :- output_to_stdout,!.

get_outputfile('user_output', _Basename, 'user_output') :- !.
get_outputfile(OutputType, _Basename, Outfile) :-
	% file search path is dev null
	user:file_search_path(OutputType, '/dev/null'),!,
	Outfile = '/dev/null'.
	
get_outputfile(OutputType, Basename, Outfile) :-	
   	Filepattern =.. [OutputType,Basename],
	absolute_file_name(Filepattern, Outfile, [access_mode(write)]).


ensure_directory(OutputFile) :-
    path_segments_atom(OutDirSegments/_File, OutputFile),
    path_segments_atom(OutDirSegments, OutDir),
    make_directory_path(OutDir).

my_RedirectStream('user_output') :-  !.
my_RedirectStream('/dev/null') :- !, open_null_stream(NullStream), tell(NullStream).
my_RedirectStream(Outfile) :- tell(Outfile).

my_CloseStream('user_output') :-!.
my_CloseStream(_Outfile) :- told.


write_null(_Q) :- format('Ignoring output~n').

%'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
