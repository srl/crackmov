
% SOURCE: http://technogems.blogspot.com/2011/08/easy-cross-platform-inter-process.html#comment-form
%   8/19/2012: Lakhotia: Taken code from above link, and merged with improvements from the comments section


:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/html_write)).
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_log)).
:- http_handler(root(handle), handle_rpc, []).

http_json:json_type('application/json-rpc').


% server - top level predicate to start the server on the default port 5555.
%   The server is UNSAFE, as in, it would evaluate any query.
%   To create juice, use the query juice_block(+Input, -Output)

server :-
	server(5555).

% server(+Port) - to start the server on port +Port.
%   the http_handler directive, above, sets up handle_rpc as the
%   callback predicate.

%  Example to perform query without going through tcp
%  S = "juice_block(block(x, [i(5, 5, or(al, ah))]), Y)",evaluate(json([jsonrpc=1, params=[S], Id=x, method=eval]), X)



server(Port) :-
	http_log_stream(_S),
	http_server(http_dispatch, [port(Port)]).

handle_rpc(Request) :-
	http_log('Request: ~p ~n', [Request]),
	http_read_json(Request, JSONIn),
	json_to_prolog(JSONIn, PrologIn),
	http_log('PrologIn: ~p~n', [PrologIn]),
	evaluate(PrologIn, PrologOut), % application body
	PrologOut = JSONOut,
	http_log('JSONOut:~p~n', JSONOut),
	reply_json(JSONOut).

evaluate(PrologIn, PrologOut) :-
	PrologIn = json([jsonrpc=Version, params=[Query], id=Id, method=MethodName]),
	MethodName = eval,
	atom_to_term(Query, Term, Bindings),
	Goal =.. [findall, Bindings, Term, IR],
	call(Goal),
	sort(IR, Result),
	jsonify(Result, JsonResult),
	atom_json_term(StringResult, JsonResult, _),
	PrologOut = json([jsonrpc=Version, result=StringResult, id=Id]).

% jsonify(+Term, -JsonTerm)
%      generates json version of a term

jsonify(Term, json(JsonTerm)) :-
	Term = [_Key=_Value|_],!,
	jsonify_x(Term, JsonTerm).
jsonify(Term, JsonTerm) :-
    is_list(Term),!,
    jsonify_x(Term, JsonTerm).
jsonify(Term, JsonTerm) :-
    swritef(JsonTerm, '%p', [Term]).

jsonify_x([], []) :-!.
jsonify_x([Key=Value| Xs], [KeyTerm=ValueTerm| Js]) :-!,
    jsonify(Key, KeyTerm),
	jsonify(Value, ValueTerm),
	jsonify_x2(Xs, Js).
jsonify_x([X|Xs], [XTerm| Js]) :-
	jsonify(X, XTerm),
	jsonify_x(Xs, Js).

jsonify_x2([], []).
jsonify_x2([Key=Value| Xs], [Key=ValueTerm| Js]) :-
    %print('---problem term ---'), nl,
    %print(Value), nl,
     jsonify_x3(Value, ValueTerm),
	 % swritef(ValueTerm, '%p', [Value]),
    %print('     --- answer --'),nl,
    %print(ValueTerm),nl,
	jsonify_x2(Xs, Js).


jsonify_x3([], []):-!.
jsonify_x3([X|Xs], [Jx|Js]) :-!,
    swritef(Jx, '%p', [X]),
    jsonify_x3(Xs, Js).
jsonify_x3(X,Y):- swritef(Y, '%p', [X]).

print_list([]) :- nl.
print_list([X|Xs]) :- print(X), nl, print_list(Xs).


%% Syntax of Result from findall 
%%    Result ::= [ Answer | _ ]
%%    Answer ::= [VarNameStr = OneAnswer | _ ]
%%    OneAnswer ::= [ field = Value | _ ]
%%    Value  ::= [ JuiceTerms | _ ]

jsonify_result(X, Y):- jasonify_result_x(X, Y).
   jsonify_result_x([], []).
   jsonify_result_x([Answer|As], [JAnswer|Js]) :-
       jsonify_answer(Answer, JAnswer),
       jsonify_result_x(As, Js).

jsonify_answer(X, Y):- jsonify_(X,Y).

server_test(TestNum):-
    test_data(TestNum, node(_Name, Block)),
    term_to_atom(juice_block(Block, _Result), Query),
    PrologIn = json([jsonrpc='dummy', params=[Query], id=xx, method=eval]),
    evaluate(PrologIn, PrologOut),
    print(PrologOut).

json_test(1, [['_G349'=[name='_text@0x1a100@0x83',
			code=[mov(eax,dptr(ebp+8)),mov(dptr(eax+4),0),mov(ecx,dptr(ebp+8)),mov(dptr(ecx),0),mov(edx,dptr(ebp+8)),mov(wptr(edx+8),0),jmp('_text@0x1a100@0x15e')],
			semantics=[eax=def(memdw(8+def(ebp))),ecx=def(memdw(8+def(ebp))),edx=def(memdw(8+def(ebp))),memdw(def(memdw(8+def(ebp))))=0,memdw(4+def(memdw(8+def(ebp))))=0,memw(8+def(memdw(8+def(ebp))))=0],
			gen_code=[mov(A,B),mov(C,D),mov(E,B),mov(F,D),mov(G,B),mov(H,D),jmp],gen_semantics=[A=def(memdw(J+def(I))),E=def(memdw(J+def(I))),G=def(memdw(J+def(I))),memdw(def(memdw(J+def(I))))=D,memdw(K+def(memdw(J+def(I))))=D,memw(J+def(memdw(J+def(I))))=D],
			gen_simplifications=[B=dptr(I+J),C=dptr(A+K),F=dptr(E),H=wptr(G+J)],gen_types=[type(A)=reg(gp),type(B)=dptr,type(C)=dptr,type(D)=int,type(E)=reg(gp),type(F)=dptr,type(G)=reg(gp),type(H)=wptr,type(I)=reg(bp),type(J)=int,type(K)=int]]]]).

json_test(2, [['_X'=[code=[x,y]]]]).

json_test(3, [['_X'=[code=[f(x),g(y)]]]]).

json_test(4, [['_X'=[code=[mov(eax,dptr(ebp+8)),mov(dptr(eax+4),0),mov(ecx,dptr(ebp+8)),mov(dptr(ecx),0),mov(edx,dptr(ebp+8)),mov(wptr(edx+8),0),jmp('_text@0x1a100@0x15e')]]]]).


json_test(5, [['_X'=[name='_text@0x1a100@0x83',code=[mov(eax,dptr(ebp+8)),mov(dptr(eax+4),0),mov(ecx,dptr(ebp+8)),mov(dptr(ecx),0),mov(edx,dptr(ebp+8)),mov(wptr(edx+8),0),jmp('_text@0x1a100@0x15e')]]]]).

json_test(6, [['_X'=[name='_text@0x1a100@0x83',code=[x,y]]]]).
json_test(7, [['_X'=[name='_text0x1a1000x83',code=[x,y]]]]).

json_test(8, [['_X'=[name='t',code=[x,y]]]]).
json_test(9, [['_X'=[name=t,code=[x,y]]]]).
json_test(10, [['_X'=[namex=t,code=[x,y]]]]).
json_test(11, [['_X'=[namex=[t(x)],code=[x,y]]]]).
json_test(12, [['_X'=[namex='_text0x1a1000x83',code=[x,y], sem=[a,b]]]]).
json_test(13, [['_X'=[namex='_text0x1a1000x83',code=[f(_X)]]]]).
json_test(14, [['_X'=[namex='_text0x1a1000x83',code=[f(X,X)]]]]).


json_test(15, [['_G349'=[name='_text@0x1a100@0x83',
			code=[mov(eax,dptr(ebp+8)),mov(dptr(eax+4),0),mov(ecx,dptr(ebp+8)),mov(dptr(ecx),0),mov(edx,dptr(ebp+8)),mov(wptr(edx+8),0),jmp('_text@0x1a100@0x15e')],
			semantics=[eax=def(memdw(8+def(ebp))),ecx=def(memdw(8+def(ebp))),edx=def(memdw(8+def(ebp))),memdw(def(memdw(8+def(ebp))))=0,memdw(4+def(memdw(8+def(ebp))))=0,memw(8+def(memdw(8+def(ebp))))=0]]]]).


json_test(16, [['_G349'=[name='_text@0x1a100@0x83',
			code=[_X=f]]]]).


